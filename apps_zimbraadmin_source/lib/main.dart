import 'dart:convert';

import 'package:apps_zimbraadmin/views/adminregional/beranda.dart';
import 'package:apps_zimbraadmin/views/adminsekolah/beranda.dart';
import 'package:apps_zimbraadmin/views/daftarsekolah.dart';
import 'package:apps_zimbraadmin/views/daftarsiswa.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'dart:io' show HttpHeaders, Platform;
import 'package:http/http.dart' as http;
import 'package:charts_flutter/flutter.dart' as charts;

import 'modal/api.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  String statusbar = "0";
  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.black,
      ));
      statusbar = "0";
    } else if (Platform.isIOS) {
      // iOS-specific code
      statusbar = "1";
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: BaseURL.focusColorTextField),
      home: MyHomePage(),
    );
  }
}

enum LoginStatus {
  notSignIn,
  signIn1,
  signIn2,
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _secureText = true;
  var _autovalidate = false;
  double screenHeight;
  var value;
  final _key = new GlobalKey<FormState>();
  LoginStatus _loginStatus = LoginStatus.notSignIn;
  String roleid, user_name;
  var loading = false;

  TextEditingController username = new TextEditingController();
  TextEditingController password = new TextEditingController();

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  signOut() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setInt('roleid', null);
      preferences.setString('username', null);
      preferences.commit();
      _loginStatus = LoginStatus.notSignIn;
    });
  }

  _login() async {
    if (username.text == "") {
      showToast("Maaf, pengguna tidak boleh kosong.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else if (password.text == "") {
      showToast("Maaf, kata sandi tidak boleh kosong.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else {
      final response = await http.post(
        BaseURL.login,
        body: {
          'username': username.text,
          'password': password.text,
        },
        headers: {
          HttpHeaders.authorizationHeader: "Bearer " + BaseURL.token,
        },
      );
      print(response.body);
      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);
        int value = data['value'];
        if (value == 0) {
          showToast(data['message'],
              gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
        } else {
          String roleid = data['roleid'];
          if (roleid == "9") {
            //admin sekolah
            _savePref(roleid, username.text);
            _loginStatus = LoginStatus.signIn1;
            username = TextEditingController(text: "");
            password = TextEditingController(text: "");
          } else if (roleid == "10") {
            //admin regional
            _savePref(roleid, username.text);
            _loginStatus = LoginStatus.signIn2;
            username = TextEditingController(text: "");
            password = TextEditingController(text: "");
          } else {
            showToast("Maaf, anda tidak diijinkan.",
                gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
          }
        }
      } else {
        showToast("Maaf, tidak ada koneksi ke server.",
            gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      }
    }
  }

  _savePref(
    String roleid,
    String username,
  ) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      preferences.setString('roleid', roleid);
      preferences.setString('username', username);
      preferences.commit();
    });
  }

  check() {
    showDialog(
      context: context,
      builder: (context) {
        Future.delayed(Duration(seconds: 1), () {
          Navigator.of(context).pop(true);
          _login();
        });
        return AlertDialog(
          title: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(),
              SizedBox(
                width: 10,
              ),
              new Text(
                "Proses ...",
                style: TextStyle(
                  fontSize: 15,
                  fontFamily: 'monserat',
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  graphik() {
    showDialog(
      context: context,
      builder: (context) {
        Future.delayed(Duration(seconds: 1), () {
          Navigator.of(context).pop(true);
          _hasilinfo();
        });
        return AlertDialog(
          title: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(),
              SizedBox(
                width: 10,
              ),
              new Text(
                "Proses ...",
                style: TextStyle(
                  fontSize: 15,
                  fontFamily: 'monserat',
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  String sd = "0", sma = "0", smk = "0", smp = "0";

  _getdatagrafik() async {
    setState(() {
      loading = true;
    });
    final response = await http.get(BaseURL.grafiklogin);
    final data = jsonDecode(response.body);
    setState(() {
      final jmlsiswa = data['jumlah_siswa'];
      sd = jmlsiswa[0];
      sma = jmlsiswa[1];
      smk = jmlsiswa[2];
      smp = jmlsiswa[3];
    });
    setState(() {
      loading = false;
    });
  }

  _hasilinfo() {
    var data1 = [
      ClicksPerYear('SD', int.parse(sd), BaseURL.colorGlobal),
      ClicksPerYear('SMP', int.parse(smp), Colors.orange[800]),
      ClicksPerYear('SMA', int.parse(sma), Colors.green[600]),
      ClicksPerYear('SMK', int.parse(smk), Colors.red[600]),
    ];

    var series1 = [
      charts.Series(
          domainFn: (ClicksPerYear clickData, _) => clickData.year,
          measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
          colorFn: (ClicksPerYear clickData, _) => clickData.color,
          id: 'Clicks',
          data: data1,
          labelAccessorFn: (ClicksPerYear clickData, _) =>
              '${clickData.clicks.toString()}'),
    ];

    var chart1 = charts.BarChart(
      series1,
      animate: true,
      vertical: true,
      barRendererDecorator: new charts.BarLabelDecorator<String>(),
    );

    var chartWidget1 = Padding(
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
        top: 20,
        bottom: 20,
      ),
      child: SizedBox(
        height: 200.0,
        child: chart1,
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(32.0))),
          contentPadding: EdgeInsets.only(top: 10.0),
          content: Container(
            width: 400.0,
            // height: 300,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Center(
                  child: Column(
                    children: [
                      Text(
                        "Jumlah siswa yang",
                        style: TextStyle(
                          fontSize: 12,
                          fontFamily: 'monserat',
                        ),
                      ),
                      Text(
                        "mendaftarkan email",
                        style: TextStyle(
                          fontSize: 12,
                          fontFamily: 'monserat',
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                chartWidget1,
                Container(
                  padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  decoration: BoxDecoration(
                    color: BaseURL.colorGlobal,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(32.0),
                      bottomRight: Radius.circular(32.0),
                    ),
                  ),
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      "TUTUP",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'monserat',
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      roleid = preferences.getString('roleid');
      user_name = preferences.getString('username');

      username = TextEditingController(text: "");
      password = TextEditingController(text: "");

      _loginStatus = roleid == "9"
          ? LoginStatus.signIn1
          : roleid == "10"
              ? LoginStatus.signIn2
              : LoginStatus.notSignIn;
    });
  }

  @override
  void initState() {
    super.initState();
    getPref();
    _getdatagrafik();
  }

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: BaseURL.statusBarColor,
      ));
    } else if (Platform.isIOS) {
      // iOS-specific code
    }
    switch (_loginStatus) {
      case LoginStatus.notSignIn:
        return Scaffold(
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/img/bg.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            child: Stack(
              children: <Widget>[
                Container(
                  child: Center(
                    child: Stack(
                      children: <Widget>[
                        ListView(
                          shrinkWrap: true,
                          padding: EdgeInsets.all(10),
                          children: <Widget>[
                            Column(
                              children: [
                                Container(
                                  height: 150,
                                  width: 150,
                                  child: Image(
                                    image: AssetImage(
                                        "assets/img/icon_e-mailsiswa-00.png"),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  "Email Siswa Nasional",
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontFamily: 'monserat',
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Center(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                  left: 15,
                                  right: 15,
                                ),
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(30.0)),
                                  elevation: 8.0,
                                  child: Container(
                                    padding: EdgeInsets.all(20.0),
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.only(top: 10),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.grey[200],
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      15.0),
                                            ),
                                            child: Padding(
                                              padding: EdgeInsets.all(1),
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 3,
                                                    bottom: 3,
                                                    left: 10,
                                                    right: 10),
                                                child: TextFormField(
                                                  controller: username,
                                                  style: new TextStyle(
                                                    fontFamily: 'monserat',
                                                  ),
                                                  keyboardType:
                                                      TextInputType.text,
                                                  autofocus: false,
                                                  decoration: InputDecoration(
                                                    border: InputBorder.none,
                                                    prefixIcon:
                                                        Icon(Icons.person),
                                                    hintText: "Pengguna",
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5.0,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(top: 10),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.grey[200],
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      15.0),
                                            ),
                                            child: Padding(
                                              padding: EdgeInsets.all(1),
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 3,
                                                    bottom: 3,
                                                    left: 10,
                                                    right: 10),
                                                child: TextFormField(
                                                  autofocus: false,
                                                  obscureText: _secureText,
                                                  controller: password,
                                                  keyboardType:
                                                      TextInputType.text,
                                                  style: new TextStyle(
                                                    fontFamily: 'monserat',
                                                  ),
                                                  decoration: InputDecoration(
                                                      border: InputBorder.none,
                                                      prefixIcon:
                                                          Icon(Icons.lock),
                                                      hintText: "Kata Sandi",
                                                      suffixIcon: IconButton(
                                                        onPressed: showHide,
                                                        icon: Icon(_secureText
                                                            ? Icons
                                                                .visibility_off
                                                            : Icons.visibility),
                                                      ),
                                                      hasFloatingPlaceholder:
                                                          true),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 15.0,
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Material(
                                              child: MaterialButton(
                                                shape: StadiumBorder(),
                                                onPressed: () => {
                                                  check(),
                                                },
                                                minWidth: 350.0,
                                                height: 50.0,
                                                color:
                                                    BaseURL.focusColorTextField,
                                                child: Text(
                                                  "MASUK",
                                                  style: TextStyle(
                                                    fontSize: 16.0,
                                                    fontFamily: 'monserat',
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Column(
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                InkWell(
                                                  onTap: () {
                                                    Navigator.of(context).push(
                                                        MaterialPageRoute(
                                                            builder: (context) =>
                                                                DaftarSekolah()));
                                                  },
                                                  child: Row(
                                                    children: [
                                                      Image(
                                                        height: 20,
                                                        width: 20,
                                                        image: AssetImage(
                                                            "assets/img/btn-adminsekolah.png"),
                                                      ),
                                                      SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        "Daftar Operator Sekolah",
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'monserat',
                                                          color: BaseURL
                                                              .colorGlobal,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),

                                                // InkWell(
                                                //   onTap: () {
                                                //     Navigator.of(context).push(
                                                //         MaterialPageRoute(
                                                //             builder: (context) =>
                                                //                 DaftarSiswa()));
                                                //   },
                                                //   child: Row(
                                                //     children: [
                                                //       Image(
                                                //         height: 20,
                                                //         width: 20,
                                                //         image: AssetImage(
                                                //             "assets/img/btn-profil.png"),
                                                //       ),
                                                //       SizedBox(
                                                //         width: 5,
                                                //       ),
                                                //       Text(
                                                //         "Daftar Siswa",
                                                //         style: TextStyle(
                                                //           fontFamily: 'monserat',
                                                //           color:
                                                //               BaseURL.colorGlobal,
                                                //         ),
                                                //       ),
                                                //     ],
                                                //   ),
                                                // ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                InkWell(
                                                  onTap: () {
                                                    // graphik();
                                                    graphik();
                                                  },
                                                  child: Row(
                                                    children: [
                                                      Icon(Icons
                                                          .graphic_eq_sharp),
                                                      SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        "Grafik Jumlah Siswa",
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'monserat',
                                                          color: BaseURL
                                                              .colorGlobal,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 15.0,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "2020 © KEMENDIKBUD - KOMINFO",
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontFamily: 'monserat',
                                  ),
                                ),
                                Text(
                                  "All rights Reserved",
                                  textScaleFactor: 1,
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontFamily: 'monserat',
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
        break;
      case LoginStatus.signIn1:
        return BerandaAdminSekolah(signOut);
        break;
      case LoginStatus.signIn2:
        return BerandaAdminRegional(signOut);
        break;
    }
  }
}

class ClicksPerYear {
  final String year;
  final int clicks;
  final charts.Color color;

  ClicksPerYear(this.year, this.clicks, Color color)
      : this.color = charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}
