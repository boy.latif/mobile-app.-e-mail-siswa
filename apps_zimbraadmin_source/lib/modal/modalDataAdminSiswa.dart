class ModalDataAdminSiswa {
  final String id;
  final String nuptk;
  final String nik;
  final String sekolah;
  final String npsn;
  final String nama;
  final String tgl_lahir;
  final String email;
  final String no_hp;
  final String password;
  final String statuss;

  ModalDataAdminSiswa(
      this.id,
      this.nuptk,
      this.nik,
      this.sekolah,
      this.npsn,
      this.nama,
      this.tgl_lahir,
      this.email,
      this.no_hp,
      this.password,
      this.statuss);
}
