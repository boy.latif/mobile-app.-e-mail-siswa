class ModalDataSiswa {
  final String id;
  final String nisn;
  final String npsn;
  final String nik;
  final String nama;
  final String alamat;
  final String sex;
  final String tgl_lahir;
  final String email;
  final String no_hp;
  final String email_alter;
  final String nama_orangtua;
  final String no_orangtua;
  final String email_orangtua;
  final String nm_sekolah;
  final String nama_region;
  final String xcreate_date;

  ModalDataSiswa(
      this.id,
      this.nisn,
      this.npsn,
      this.nik,
      this.nama,
      this.alamat,
      this.sex,
      this.tgl_lahir,
      this.email,
      this.no_hp,
      this.email_alter,
      this.nama_orangtua,
      this.no_orangtua,
      this.email_orangtua,
      this.nm_sekolah,
      this.nama_region,
      this.xcreate_date);
}
