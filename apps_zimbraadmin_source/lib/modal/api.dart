import 'package:flutter/material.dart';

class BaseURL {
  static Color statusBarColor = Colors.black;
  static Color focusColorTextField = Color.fromRGBO(115, 103, 240, 1);
  static Color colorGlobal = Color.fromRGBO(115, 103, 240, 1);

  // static String ip = "http://live.intens.id/siswa_api/api/";
  static String ip = "https://api.layanan.go.id:8243/emailsiswa/v1.0.0/";
  static String token = "e71c828a-535b-396d-8490-3d68db952327";
  static String cekdata_adminsekolah = ip + "sekolah_new?nuptk=";
  static String simpan_adminsekolah = ip + "sekolah_new";
  static String cekdata_emailsiswa = ip + "siswa_new?nisn=";
  static String simpan_emailsiswa = ip + "siswa_new";
  static String login = ip + "loginn";
  static String lupakatasandi_sekolah = ip + "lupakatasandi_sekolah?nuptk=";
  static String resetkey = ip + "lupakatasandi_sekolah";
  static String grafiklogin =
      "https://emailsiswa.layanan.go.id/login_portal/get_statistik";

  // data akses sekolah
  static String getinfo_sekolah = ip + "getinfo_sekolah?nuptk=";
  static String getinfo_grafikberdasarkansekolah =
      ip + "getinfo_grafikberdasarkansekolah?id_sekolah=";
  static String cekdata_daftaremailsiswa =
      ip + "checkdata_daftaremailsiswa?nisn=";
  static String simpan_daftaremailsiswa = ip + "data_siswa";
  static String getdata_siswa = ip + "getdata_siswa?id_sekolah=";
  static String lihatdata_detailsiswa = ip + "data_siswa?id=";
  static String ubahkata_sandi = ip + "login";
  static String hapussiswa = ip + "siswa_new";

  //edit
  static String show_siswa = ip + "show_siswa?nisn=";
  static String siswa_update_new = ip + "siswa_update_new";
  static String editoptr_sekolah = ip + "editoptr_sekolah";
  static String deleteoptr_sekolah = ip + "deleteoptr_sekolah";
  static String resetpassword_siswa = ip + "resetpassword_siswa";
  static String siswa_delete_new = ip + "siswa_delete_new";

  //data akses regional
  static String getinfo_regional = ip + "getinfo_regional?username=";
  static String getlist_sekolahregional =
      ip + "getlist_sekolahregional?id_region=";
  static String grafiksemua_sekolah1 = ip + "grafiksemua_sekolah1?id_region=";
  static String grafiksemua_jeniskelamin =
      ip + "grafiksemua_jeniskelamin?id_region=";
  static String grafiksemua_jenjang = ip + "grafiksemua_jenjang?id_region=";
  static String grafiksekolah = ip + "grafiksekolah?id_regional=";
  static String getDataAdminRegional = ip + "getdataadminregional?id_region=";
  static String ubahStatus = ip + "ubahstatus";
}
