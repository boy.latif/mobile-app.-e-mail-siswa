import 'dart:convert';
import 'dart:io';

import 'package:apps_zimbraadmin/modal/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;

class UbahPassword extends StatefulWidget {
  final String id;
  final String roleid;
  final VoidCallback signOut;
  UbahPassword(this.id, this.roleid, this.signOut);
  @override
  _UbahPasswordState createState() => _UbahPasswordState();
}

class _UbahPasswordState extends State<UbahPassword> {
  bool _secureText = true;
  var _autovalidate = false;
  double screenHeight;
  var value;
  String username = null, password = null;
  final _key = new GlobalKey<FormState>();
  TextEditingController katasandi_saatini = new TextEditingController();
  TextEditingController katasandi_baru = new TextEditingController();
  TextEditingController ketikulang_katasandi = new TextEditingController();

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  signOut() {
    setState(() {
      widget.signOut();
    });
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  ubahkatasandi() async {
    final response = await http.post(
      BaseURL.ubahkata_sandi,
      body: {
        'katasandi_saatini': katasandi_saatini.text,
        'katasandi_baru': katasandi_baru.text,
        'ketikulang_katasandi': ketikulang_katasandi.text,
        'username': widget.id,
        'roleid': widget.roleid,
      },
      headers: {
        HttpHeaders.authorizationHeader: "Bearer " + BaseURL.token,
      },
    );
    final data = jsonDecode(response.body);
    int value = data['value'];
    String pesan = data['message'];
    if (value == 0) {
      showToast(pesan, gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else {
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            contentPadding: EdgeInsets.only(top: 10.0),
            content: Container(
              width: 400.0,
              height: 250,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    height: 100,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/img/editpass.png'),
                        fit: BoxFit.fitHeight,
                      ),
                      shape: BoxShape.circle,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Expanded(
                    child: ListView(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Center(
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text(
                                  "Selamat, kata sandi anda berhasil dirubah, akun akan keluar dengan sendirinya, silahkan anda login kembali.",
                                  style: TextStyle(
                                    fontFamily: 'monserat',
                                    fontSize: 13,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                    decoration: BoxDecoration(
                      color: BaseURL.colorGlobal,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(32.0),
                        bottomRight: Radius.circular(32.0),
                      ),
                    ),
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          Navigator.pop(context);
                          signOut();
                          Navigator.pop(context);
                        });
                      },
                      child: Text(
                        "TUTUP",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'monserat',
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );
    }
  }

  prosesdata() {
    showDialog(
      context: context,
      builder: (context) {
        Future.delayed(Duration(seconds: 1), () {
          Navigator.of(context).pop(true);
          ubahkatasandi();
        });
        return AlertDialog(
          title: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(),
              SizedBox(
                width: 10,
              ),
              new Text(
                "Proses ...",
                style: TextStyle(
                  fontSize: 15,
                  fontFamily: 'monserat',
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  _hasilinfo() {
    if (katasandi_saatini.text == "") {
      showToast("Maaf, Kata sandi saat ini tidak boleh kosong.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else if (katasandi_baru.text == "") {
      showToast("Maaf, Kata sandi baru tidak boleh kosong.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else if (ketikulang_katasandi.text == "") {
      showToast("Maaf, Data ketik ulang kata sandi tidak boleh kosong.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else {
      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(32.0))),
            contentPadding: EdgeInsets.only(top: 10.0),
            content: Container(
              width: 400.0,
              height: 250,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    height: 100,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('assets/img/editpass.png'),
                        fit: BoxFit.fitHeight,
                      ),
                      shape: BoxShape.circle,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Expanded(
                    child: ListView(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Center(
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Text(
                                  "Apakah anda yakin, akan memperbarui kata sandi anda?",
                                  style: TextStyle(
                                    fontFamily: 'monserat',
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                    decoration: BoxDecoration(
                      color: BaseURL.colorGlobal,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(32.0),
                        bottomRight: Radius.circular(32.0),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            prosesdata();
                          },
                          child: Text(
                            "PERBARUI",
                            style: TextStyle(
                              color: Colors.yellow[500],
                              fontFamily: 'monserat',
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "TIDAK",
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'monserat',
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );
    }
  }

  String statusbar = "0";

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.black,
      ));
      statusbar = "0";
    } else if (Platform.isIOS) {
      // iOS-specific code
      statusbar = "1";
    }
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: BaseURL.colorGlobal,
      ),
      home: Scaffold(
        appBar: AppBar(
          brightness: statusbar == "1" ? Brightness.light : Brightness.dark,
          backgroundColor: Colors.white,
          toolbarHeight: 60,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      height: 25,
                      width: 25,
                      decoration: BoxDecoration(
                        color: BaseURL.colorGlobal,
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.black54,
                              blurRadius: 3.0,
                              offset: Offset(0.5, 1))
                        ],
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                      child: Center(
                        child: Icon(
                          Icons.arrow_back,
                          size: 18,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Ubah Kata Sandi",
                            style: TextStyle(
                              fontSize: 18.0,
                              fontFamily: 'monserat',
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: new BorderRadius.circular(15.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(1),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 3, bottom: 3, left: 10, right: 10),
                      child: TextFormField(
                        autofocus: false,
                        obscureText: _secureText,
                        controller: katasandi_saatini,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            prefixIcon: Icon(Icons.lock),
                            hintText: "Kata sandi saat ini",
                            hintStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            hasFloatingPlaceholder: true),
                        style: TextStyle(
                          fontFamily: 'monserat',
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: new BorderRadius.circular(15.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(1),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 3, bottom: 3, left: 10, right: 10),
                      child: TextFormField(
                        autofocus: false,
                        obscureText: _secureText,
                        controller: katasandi_baru,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            prefixIcon: Icon(Icons.lock),
                            hintText: "Kata sandi baru",
                            hintStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            hasFloatingPlaceholder: true),
                        style: TextStyle(
                          fontFamily: 'monserat',
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: new BorderRadius.circular(15.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(1),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 3, bottom: 3, left: 10, right: 10),
                      child: TextFormField(
                        autofocus: false,
                        obscureText: _secureText,
                        controller: ketikulang_katasandi,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            prefixIcon: Icon(Icons.lock),
                            hintText: "Ketik ulang kata sandi",
                            hintStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            hasFloatingPlaceholder: true),
                        style: TextStyle(
                          fontFamily: 'monserat',
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Column(
                children: <Widget>[
                  Material(
                    child: MaterialButton(
                      shape: StadiumBorder(),
                      onPressed: () => {
                        _hasilinfo(),
                      },
                      height: 50.0,
                      color: BaseURL.colorGlobal,
                      child: Text(
                        "PERBARUI",
                        style: TextStyle(
                          fontSize: 16.0,
                          fontFamily: 'monserat',
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
