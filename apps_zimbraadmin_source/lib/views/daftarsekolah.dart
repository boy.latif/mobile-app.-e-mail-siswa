import 'dart:convert';
import 'dart:io';

import 'package:apps_zimbraadmin/modal/api.dart';
import 'package:apps_zimbraadmin/views/lupa_katasandi.dart';
import 'package:apps_zimbraadmin/views/reset_katakunci.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;

class DaftarSekolah extends StatefulWidget {
  @override
  _DaftarSekolahState createState() => _DaftarSekolahState();
}

class _DaftarSekolahState extends State<DaftarSekolah> {
  bool _secureText = true;
  var _autovalidate = false;
  double screenHeight;
  var value;
  String username = null, password = null;
  final _key = new GlobalKey<FormState>();

  TextEditingController nuptk = new TextEditingController();
  TextEditingController nik = new TextEditingController();
  TextEditingController nama = new TextEditingController();
  TextEditingController nama_sekolah = new TextEditingController();
  TextEditingController email_alternatif = new TextEditingController();
  TextEditingController nohp = new TextEditingController();
  TextEditingController katasandi = new TextEditingController();
  TextEditingController idsekolah = new TextEditingController();
  TextEditingController idregion = new TextEditingController();

  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }

  _proses() {
    showDialog(
      context: context,
      builder: (context) {
        Future.delayed(Duration(seconds: 1), () {
          Navigator.of(context).pop(true);
          _check();
        });
        return AlertDialog(
          title: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(),
              SizedBox(
                width: 10,
              ),
              new Text(
                "Proses ...",
                style: TextStyle(
                  fontSize: 15,
                  fontFamily: 'monserat',
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  _proses_simpan() {
    showDialog(
      context: context,
      builder: (context) {
        Future.delayed(Duration(seconds: 1), () {
          Navigator.of(context).pop(true);
          _simpan();
        });
        return AlertDialog(
          title: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(),
              SizedBox(
                width: 10,
              ),
              new Text(
                "Proses ...",
                style: TextStyle(
                  fontSize: 15,
                  fontFamily: 'monserat',
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  _simpan() async {
    if (email_alternatif.text == "") {
      showToast("Maaf, email alternatif tidak boleh kosong.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else if (nohp.text == "") {
      showToast("Maaf, no HP tidak boleh kosong.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else if (katasandi.text == "") {
      showToast("Maaf, password tidak boleh kosong.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else {
      final response = await http.post(
        BaseURL.simpan_adminsekolah,
        body: {
          'nuptk': nuptk.text,
          'nik': nik.text,
          'nama': nama.text,
          'nama_sekolah': nama_sekolah.text,
          'email_alternatif': email_alternatif.text,
          'nohp': nohp.text,
          'password': katasandi.text,
          'idsekolah': idsekolah.text,
          'idregion': idregion.text,
        },
        headers: {
          HttpHeaders.authorizationHeader: "Bearer " + BaseURL.token,
        },
      );
      final data = jsonDecode(response.body);
      int value = data['value'];
      String pesan = data['message'];
      if (value == 1) {
        Navigator.pop(context);
        _hasilinfo();
      } else if (value == 0) {
        showToast(pesan, gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      }
    }
  }

  _hasilinfo() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(32.0))),
          contentPadding: EdgeInsets.only(top: 10.0),
          content: Container(
            width: 400.0,
            height: 300,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: 100,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/img/confirm_check.png'),
                      fit: BoxFit.fitHeight,
                    ),
                    shape: BoxShape.circle,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: ListView(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Center(
                        child: Column(
                          children: [
                            Text(
                              "Data Berhasil Disimpan.",
                              style: TextStyle(
                                fontFamily: 'monserat',
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Column(
                                    children: [
                                      Text(
                                        "Username : " + nuptk.text,
                                        style: TextStyle(
                                          fontFamily: 'monserat',
                                          fontSize: 12,
                                        ),
                                      ),
                                      Text(
                                        "Password : " + katasandi.text,
                                        style: TextStyle(
                                          fontFamily: 'monserat',
                                          fontSize: 12,
                                        ),
                                      ),
                                      Text(
                                        "Email Alternatif : " +
                                            email_alternatif.text,
                                        style: TextStyle(
                                          fontFamily: 'monserat',
                                          fontSize: 12,
                                        ),
                                      ),
                                      Text(
                                        "*Jangan lupa discreenshoot!",
                                        style: TextStyle(
                                          fontFamily: 'monserat',
                                          fontSize: 12,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  decoration: BoxDecoration(
                    color: BaseURL.colorGlobal,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(32.0),
                      bottomRight: Radius.circular(32.0),
                    ),
                  ),
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    child: Text(
                      "TUTUP",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'monserat',
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  openInfoCheckBox() {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(32.0))),
          contentPadding: EdgeInsets.only(top: 10.0),
          content: Container(
            width: 400.0,
            height: 530,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/img/check.png'),
                      fit: BoxFit.fitHeight,
                    ),
                    shape: BoxShape.circle,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: ListView(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: new BorderRadius.circular(15.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20, right: 20),
                            child: TextFormField(
                              controller: nama,
                              decoration: InputDecoration(
                                hintText: "Nama",
                                hintStyle: TextStyle(
                                  fontFamily: 'monserat',
                                ),
                                enabled: false,
                                border: InputBorder.none,
                              ),
                              keyboardType: TextInputType.text,
                              style: new TextStyle(
                                fontFamily: 'monserat',
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: new BorderRadius.circular(15.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20, right: 20),
                            child: TextFormField(
                              controller: nama_sekolah,
                              decoration: InputDecoration(
                                hintText: "Nama Sekolah",
                                hintStyle: TextStyle(
                                  fontFamily: 'monserat',
                                ),
                                enabled: false,
                                border: InputBorder.none,
                              ),
                              keyboardType: TextInputType.text,
                              style: new TextStyle(
                                fontFamily: 'monserat',
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: new BorderRadius.circular(15.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20, right: 20),
                            child: TextFormField(
                              controller: email_alternatif,
                              decoration: InputDecoration(
                                hintText: "Email Alternatif",
                                hintStyle: TextStyle(
                                  fontFamily: 'monserat',
                                ),
                                border: InputBorder.none,
                              ),
                              keyboardType: TextInputType.emailAddress,
                              style: new TextStyle(
                                fontFamily: 'monserat',
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: new BorderRadius.circular(15.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20, right: 20),
                            child: TextFormField(
                              controller: nohp,
                              decoration: InputDecoration(
                                hintText: "No HP",
                                hintStyle: TextStyle(
                                  fontFamily: 'monserat',
                                ),
                                border: InputBorder.none,
                              ),
                              keyboardType: TextInputType.number,
                              style: new TextStyle(
                                fontFamily: 'monserat',
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 10, right: 10),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.grey[200],
                            borderRadius: new BorderRadius.circular(15.0),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(
                              left: 20,
                              right: 20,
                            ),
                            child: TextFormField(
                              obscureText: _secureText,
                              controller: katasandi,
                              decoration: InputDecoration(
                                hintText: "Password",
                                hintStyle: TextStyle(
                                  fontFamily: 'monserat',
                                ),
                                border: InputBorder.none,
                              ),
                              keyboardType: TextInputType.text,
                              style: new TextStyle(
                                fontFamily: 'monserat',
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  decoration: BoxDecoration(
                    color: BaseURL.colorGlobal,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(32.0),
                      bottomRight: Radius.circular(32.0),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      InkWell(
                        onTap: () {
                          _proses_simpan();
                        },
                        child: Text(
                          "SIMPAN",
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'monserat',
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          // _hasilinfo();
                        },
                        child: Text(
                          "BATAL",
                          style: TextStyle(
                            color: Colors.yellow[200],
                            fontFamily: 'monserat',
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  _check() async {
    if (nuptk.text == "") {
      showToast("Maaf, nuptk tidak boleh kosong.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else if (nik.text == "") {
      showToast("Maaf, nik tidak boleh kosong.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else {
      final response = await http.get(
        BaseURL.cekdata_adminsekolah + nuptk.text + "&nik=" + nik.text,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + BaseURL.token,
        },
      );
      final data = jsonDecode(response.body);
      if (data['value'] == 0) {
        showToast(data['message'],
            gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      } else {
        final result = data['data'];
        setState(() {
          nama = TextEditingController(text: result['nama']);
          nama_sekolah = TextEditingController(text: result['nama_sekolah']);
          idsekolah = TextEditingController(text: result['id_sekolah']);
          idregion = TextEditingController(text: result['id_region']);
        });
        openInfoCheckBox();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: BaseURL.statusBarColor,
      ));
    } else if (Platform.isIOS) {
      // iOS-specific code
    }
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/img/bg.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget>[
            Container(
              child: Center(
                child: SingleChildScrollView(
                  child: Stack(
                    children: <Widget>[
                      ListView(
                        shrinkWrap: true,
                        padding: EdgeInsets.all(10),
                        children: <Widget>[
                          Column(
                            children: [
                              Text(
                                "Daftar Operator Sekolah",
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: 'monserat',
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Form(
                            autovalidate: _autovalidate,
                            key: _key,
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                  left: 15,
                                  right: 15,
                                ),
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(30.0)),
                                  elevation: 8.0,
                                  child: Container(
                                    padding: EdgeInsets.all(20.0),
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.only(top: 10),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.grey[200],
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      15.0),
                                            ),
                                            child: Padding(
                                              padding: EdgeInsets.all(1),
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 3,
                                                    bottom: 3,
                                                    left: 10,
                                                    right: 10),
                                                child: TextFormField(
                                                  controller: nuptk,
                                                  keyboardType:
                                                      TextInputType.number,
                                                  style: new TextStyle(
                                                    fontFamily: 'monserat',
                                                  ),
                                                  autofocus: false,
                                                  decoration: InputDecoration(
                                                    border: InputBorder.none,
                                                    prefixIcon:
                                                        Icon(Icons.credit_card),
                                                    hintText: "NUPTK",
                                                    hintStyle: TextStyle(
                                                      fontFamily: 'monserat',
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(top: 10),
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.grey[200],
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      15.0),
                                            ),
                                            child: Padding(
                                              padding: EdgeInsets.all(1),
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 3,
                                                    bottom: 3,
                                                    left: 10,
                                                    right: 10),
                                                child: TextFormField(
                                                  controller: nik,
                                                  keyboardType:
                                                      TextInputType.number,
                                                  style: new TextStyle(
                                                    fontFamily: 'monserat',
                                                  ),
                                                  autofocus: false,
                                                  decoration: InputDecoration(
                                                    border: InputBorder.none,
                                                    prefixIcon:
                                                        Icon(Icons.credit_card),
                                                    hintText: "NIK",
                                                    hintStyle: TextStyle(
                                                      fontFamily: 'monserat',
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 15.0,
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Material(
                                              child: MaterialButton(
                                                shape: StadiumBorder(),
                                                onPressed: () => {
                                                  _proses(),
                                                },
                                                minWidth: 350.0,
                                                height: 50.0,
                                                color:
                                                    BaseURL.focusColorTextField,
                                                child: Text(
                                                  "CHECK",
                                                  style: TextStyle(
                                                    fontSize: 16.0,
                                                    fontFamily: 'monserat',
                                                    color: Colors.white,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        Column(
                                          children: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Row(
                                                  children: [
                                                    Text(
                                                      "Lupa kata sandi ?",
                                                      style: TextStyle(
                                                        fontFamily: 'monserat',
                                                      ),
                                                    ),
                                                    InkWell(
                                                      onTap: () {
                                                        Navigator.of(context).push(
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        LupaKataSandi()));
                                                      },
                                                      child: Text(
                                                        " Klik disini!",
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'monserat',
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: BaseURL
                                                              .colorGlobal,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Row(
                                                  children: [
                                                    Text(
                                                      "Reset Kata Kunci ?",
                                                      style: TextStyle(
                                                        fontFamily: 'monserat',
                                                      ),
                                                    ),
                                                    InkWell(
                                                      onTap: () {
                                                        Navigator.of(context).push(
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        ResetKataKunci()));
                                                      },
                                                      child: Text(
                                                        " Klik disini!",
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'monserat',
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: BaseURL
                                                              .colorGlobal,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: [
                                                Row(
                                                  children: [
                                                    Text(
                                                      "Sudah punya akun ?",
                                                      style: TextStyle(
                                                        fontFamily: 'monserat',
                                                      ),
                                                    ),
                                                    InkWell(
                                                      onTap: () {
                                                        Navigator.pop(context);
                                                      },
                                                      child: Text(
                                                        " Klik disini!",
                                                        style: TextStyle(
                                                          fontFamily:
                                                              'monserat',
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: BaseURL
                                                              .colorGlobal,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "2020 © KEMENDIKBUD - KOMINFO",
                                textScaleFactor: 1,
                                style: TextStyle(
                                  fontSize: 15,
                                  fontFamily: 'monserat',
                                ),
                              ),
                              Text(
                                "All rights Reserved",
                                textScaleFactor: 1,
                                style: TextStyle(
                                  fontSize: 15,
                                  fontFamily: 'monserat',
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
