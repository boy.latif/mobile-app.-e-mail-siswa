import 'dart:io';

import 'package:apps_zimbraadmin/modal/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DaftarAdminSekolah extends StatefulWidget {
  @override
  _DaftarAdminSekolahState createState() => _DaftarAdminSekolahState();
}

class _DaftarAdminSekolahState extends State<DaftarAdminSekolah> {
  bool _secureText = true;
  var _autovalidate = false;
  double screenHeight;
  var value;
  String username = null, password = null;
  final _key = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: BaseURL.statusBarColor,
      ));
    } else if (Platform.isIOS) {
      // iOS-specific code
    }
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Row(
            children: [
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "Daftar Operator Sekolah",
                style: TextStyle(
                  fontSize: 16.0,
                  fontFamily: 'monserat',
                  color: Colors.black,
                ),
              ),
            ],
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListView(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: new BorderRadius.circular(15.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(1),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 3, bottom: 3, left: 10, right: 10),
                      child: TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Silahkan masukkan username";
                          } else {
                            return null;
                          }
                        },
                        onSaved: (e) => username = e,
                        keyboardType: TextInputType.text,
                        autofocus: false,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          prefixIcon: Icon(Icons.credit_card),
                          hintText: "NUPTK",
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: new BorderRadius.circular(15.0),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(1),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 3, bottom: 3, left: 10, right: 10),
                      child: TextFormField(
                        validator: (e) {
                          if (e.isEmpty) {
                            return "Silahkan masukkan username";
                          } else {
                            return null;
                          }
                        },
                        onSaved: (e) => username = e,
                        keyboardType: TextInputType.text,
                        autofocus: false,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          prefixIcon: Icon(Icons.credit_card),
                          hintText: "NIK",
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Column(
                children: <Widget>[
                  Material(
                    child: MaterialButton(
                      shape: StadiumBorder(),
                      onPressed: () => {
                        // check(),
                        // Navigator.of(context)
                        //     .pushReplacement(
                        //         MaterialPageRoute(
                        //             builder: (context) =>
                        //                 Beranda(
                        //                     signOut))),
                      },
                      // minWidth: 350.0,
                      height: 50.0,
                      color: BaseURL.colorGlobal,
                      child: Text(
                        "CHECK",
                        style: TextStyle(
                          fontSize: 16.0,
                          fontFamily: 'monserat',
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
