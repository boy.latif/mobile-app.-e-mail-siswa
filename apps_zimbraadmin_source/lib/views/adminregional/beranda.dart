import 'dart:convert';
import 'dart:io';

import 'package:apps_zimbraadmin/main.dart';
import 'package:apps_zimbraadmin/modal/api.dart';
import 'package:apps_zimbraadmin/modal/modalGrafikEmailSekolah.dart';
import 'package:apps_zimbraadmin/modal/modalRegional.dart';
import 'package:apps_zimbraadmin/views/adminregional/daftaradminsekolah.dart';
import 'package:apps_zimbraadmin/views/adminregional/listadmin.dart';
import 'package:apps_zimbraadmin/views/ubahpassword.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class BerandaAdminRegional extends StatefulWidget {
  final VoidCallback signOut;
  BerandaAdminRegional(this.signOut);
  @override
  _BerandaAdminRegionalState createState() => _BerandaAdminRegionalState();
}

class _BerandaAdminRegionalState extends State<BerandaAdminRegional> {
  var jenisKelamin;
  String userid,
      name,
      username,
      nama_region,
      nama_awal,
      roleid,
      id_region,
      idsekolah = "0",
      statusgrafik = "0";
  int laki = 0,
      perempuan = 0,
      sd = 0,
      smp = 0,
      sma = 0,
      sudah = 0,
      belum = 0,
      laki2 = 0,
      perempuan2 = 0;
  List<ModalRegional> _listRegional = [];
  List<ModalGrafikEmailSekolah> datagrafik1 = [];

  _keluarAplikasi() {
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          child: ListView(
            padding: EdgeInsets.all(16),
            shrinkWrap: true,
            children: <Widget>[
              Text(
                "Keluar dari Aplikasi Email Siswa Nasional?",
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'monserat',
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Apakah anda yakin ingin keluar?",
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'monserat',
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Tidak",
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'Quicksand',
                        ),
                      )),
                  SizedBox(
                    width: 16,
                  ),
                  InkWell(
                      onTap: () {
                        Navigator.pop(context);
                        widget.signOut();
                      },
                      child: Text(
                        "Keluar",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          fontFamily: 'Quicksand',
                          color: BaseURL.colorGlobal,
                        ),
                      )),
                ],
              )
            ],
          ),
        );
      },
    );
  }

  signOut() {
    setState(() {
      widget.signOut();
    });
  }

  getinfo_regional() async {
    final response = await http.get(
      BaseURL.getinfo_regional + username,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + BaseURL.token,
      },
    );
    final data = jsonDecode(response.body);
    setState(() {
      userid = data['userid'];
      name = data['name'];
      nama_region = data['nama_region'];
      nama_awal = data['nama_awal'];
      id_region = data['id_region'];
    });
    _getDataSekolahRegional();
    _datagrafik1();
    _datagrafikJenisKelamin();
    _datagrafikJenjang();
  }

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      roleid = preferences.getString('roleid');
      username = preferences.getString('username');
    });
    getinfo_regional();
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  _datagrafikSekolah() async {
    final response = await http.get(
      BaseURL.grafiksekolah + id_region + "&id_sekolah=" + idsekolah,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + BaseURL.token,
      },
    );
    final data = jsonDecode(response.body);
    setState(() {
      sudah = data['sudah'];
      belum = data['belum'];
      laki2 = data['laki'];
      perempuan2 = data['perempuan'];
    });
  }

  _datagrafik1() async {
    final response = await http.get(
      BaseURL.grafiksemua_sekolah1 + id_region,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + BaseURL.token,
      },
    );
    final data = jsonDecode(response.body);
    final result = data['data'];
    setState(() {
      result.forEach((api) {
        final ab = new ModalGrafikEmailSekolah(
          api['nm_sekolah'],
          int.parse(api['jml']),
        );
        datagrafik1.add(ab);
      });
    });
  }

  _datagrafikJenisKelamin() async {
    final response = await http.get(
      BaseURL.grafiksemua_jeniskelamin + id_region,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + BaseURL.token,
      },
    );
    final data = jsonDecode(response.body);
    setState(() {
      laki = data['laki'];
      perempuan = data['perempuan'];
    });
  }

  _datagrafikJenjang() async {
    final response = await http.get(
      BaseURL.grafiksemua_jenjang + id_region,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + BaseURL.token,
      },
    );
    final data = jsonDecode(response.body);
    setState(() {
      sd = data['sd'];
      smp = data['smp'];
      sma = data['sma'];
    });
  }

  Widget datapersekolah() {
    var data1 = [
      ClicksPerYear('Sudah', sudah, BaseURL.colorGlobal),
      ClicksPerYear('Belum', belum, BaseURL.colorGlobal),
    ];

    var series1 = [
      charts.Series(
          domainFn: (ClicksPerYear clickData, _) => clickData.year,
          measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
          colorFn: (ClicksPerYear clickData, _) => clickData.color,
          id: 'Clicks',
          data: data1,
          labelAccessorFn: (ClicksPerYear clickData, _) =>
              '${clickData.clicks.toString()}'),
    ];

    var chart1 = charts.BarChart(
      series1,
      animate: true,
      vertical: true,
      barRendererDecorator: new charts.BarLabelDecorator<String>(),
    );

    var chartWidget1 = Padding(
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
        top: 20,
        bottom: 20,
      ),
      child: SizedBox(
        height: 200.0,
        child: chart1,
      ),
    );

    var data2 = [
      ClicksPerYear('Laki-laki', laki2, BaseURL.colorGlobal),
      ClicksPerYear('Perempuan', perempuan2, BaseURL.colorGlobal),
    ];

    var series2 = [
      charts.Series(
          domainFn: (ClicksPerYear clickData, _) => clickData.year,
          measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
          colorFn: (ClicksPerYear clickData, _) => clickData.color,
          id: 'Clicks',
          data: data2,
          labelAccessorFn: (ClicksPerYear clickData, _) =>
              '${clickData.clicks.toString()}'),
    ];

    var chart2 = charts.BarChart(
      series2,
      animate: true,
      vertical: true,
      barRendererDecorator: new charts.BarLabelDecorator<String>(),
    );

    var chartWidget2 = Padding(
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
        top: 20,
        bottom: 20,
      ),
      child: SizedBox(
        height: 200.0,
        child: chart2,
      ),
    );
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Text(
            "Perbandingan data siswa yang telah mendaftar berdasarkan sekolah",
            style: TextStyle(
              // fontSize: 12,
              color: Colors.grey[700],
              fontFamily: 'monserat',
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 2),
                ),
              ],
            ),
            child: chartWidget1,
          ),
        ),
        SizedBox(
          height: 30,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Text(
            "Perbandingan data siswa yang telah mendaftar berdasarkan jenis kelamin",
            style: TextStyle(
              // fontSize: 12,
              color: Colors.grey[700],
              fontFamily: 'monserat',
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 2),
                ),
              ],
            ),
            child: chartWidget2,
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  Widget semuasekolah() {
    var databaru = [ClicksPerYear("", 0, BaseURL.colorGlobal)];

    if (datagrafik1.length > 0) {
      databaru.clear();
      for (var i = 0; i < datagrafik1.length; i++) {
        final x = datagrafik1[i];
        databaru.add(
          ClicksPerYear(x.nm_sekolah, x.jml, BaseURL.colorGlobal),
        );
      }
    }

    var series = [
      charts.Series(
          domainFn: (ClicksPerYear clickData, _) => clickData.year,
          measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
          colorFn: (ClicksPerYear clickData, _) => clickData.color,
          id: 'Clicks',
          data: databaru,
          labelAccessorFn: (ClicksPerYear clickData, _) =>
              '${clickData.clicks.toString()}'),
    ];

    var chart = charts.BarChart(
      series,
      animate: true,
      vertical: idsekolah == "0" ? false : true,
      barRendererDecorator: new charts.BarLabelDecorator<String>(),
    );

    var chartWidget = Padding(
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
        top: 20,
        bottom: 20,
      ),
      child: SizedBox(
        height: 300.0,
        child: chart,
      ),
    );

    var data2 = [
      ClicksPerYear('Laki-laki', laki, BaseURL.colorGlobal),
      ClicksPerYear('Perempuan', perempuan, BaseURL.colorGlobal),
    ];

    var series2 = [
      charts.Series(
          domainFn: (ClicksPerYear clickData, _) => clickData.year,
          measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
          colorFn: (ClicksPerYear clickData, _) => clickData.color,
          id: 'Clicks',
          data: data2,
          labelAccessorFn: (ClicksPerYear clickData, _) =>
              '${clickData.clicks.toString()}'),
    ];

    var chart2 = charts.BarChart(
      series2,
      animate: true,
      vertical: true,
      barRendererDecorator: new charts.BarLabelDecorator<String>(),
    );

    var chartWidget2 = Padding(
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
        top: 20,
        bottom: 20,
      ),
      child: SizedBox(
        height: 200.0,
        child: chart2,
      ),
    );

    var data3 = [
      ClicksPerYear('SD', sd, BaseURL.colorGlobal),
      ClicksPerYear('SMP', smp, BaseURL.colorGlobal),
      ClicksPerYear('SMA', sma, BaseURL.colorGlobal),
    ];

    var series3 = [
      charts.Series(
          domainFn: (ClicksPerYear clickData, _) => clickData.year,
          measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
          colorFn: (ClicksPerYear clickData, _) => clickData.color,
          id: 'Clicks',
          data: data3,
          labelAccessorFn: (ClicksPerYear clickData, _) =>
              '${clickData.clicks.toString()}'),
    ];

    var chart3 = charts.BarChart(
      series3,
      animate: true,
      vertical: true,
      barRendererDecorator: new charts.BarLabelDecorator<String>(),
    );

    var chartWidget3 = Padding(
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
        top: 20,
        bottom: 20,
      ),
      child: SizedBox(
        height: 200.0,
        child: chart3,
      ),
    );

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Text(
            "Pendaftar email siswa berdasarkan sekolah",
            style: TextStyle(
              // fontSize: 12,
              color: Colors.grey[700],
              fontFamily: 'monserat',
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 2),
                ),
              ],
            ),
            child: chartWidget,
          ),
        ),
        SizedBox(
          height: 30,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Text(
            "Pendaftar email siswa berdasarkan jenis kelamin",
            style: TextStyle(
              // fontSize: 12,
              color: Colors.grey[700],
              fontFamily: 'monserat',
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 2),
                ),
              ],
            ),
            child: chartWidget2,
          ),
        ),
        SizedBox(
          height: 30,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Text(
            "Pendaftar email siswa berdasarkan jenjang",
            style: TextStyle(
              // fontSize: 12,
              color: Colors.grey[700],
              fontFamily: 'monserat',
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                bottomLeft: Radius.circular(10.0),
                bottomRight: Radius.circular(10.0),
                topRight: Radius.circular(10.0),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 5,
                  offset: Offset(0, 2),
                ),
              ],
            ),
            child: chartWidget3,
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }

  Future<void> _getDataSekolahRegional() async {
    final response = await http.get(
      BaseURL.getlist_sekolahregional + id_region,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + BaseURL.token,
      },
    );
    final data = jsonDecode(response.body);
    final result = data['data'];
    setState(() {
      _listRegional.add(new ModalRegional(
        data['id_sekolah'],
        data['nama_sekolah'],
        data['jenjang'],
      ));
      result.forEach((api) {
        final a = new ModalRegional(
          api['id_sekolah'],
          api['nm_sekolah'],
          api['jenjang'],
        );
        _listRegional.add(a);
      });
    });
  }

  String statusbar = "0";

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.black,
      ));
      statusbar = "0";
    } else if (Platform.isIOS) {
      // iOS-specific code
      statusbar = "1";
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          brightness: statusbar == "1" ? Brightness.light : Brightness.dark,
          backgroundColor: Colors.white,
          toolbarHeight: 70,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                      color: BaseURL.colorGlobal,
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.black54,
                            blurRadius: 3.0,
                            offset: Offset(0.5, 1))
                      ],
                      borderRadius: BorderRadius.circular(50.0),
                    ),
                    child: Center(
                      child: Text(
                        "$nama_awal",
                        style: TextStyle(
                          fontSize: 20.0,
                          fontFamily: 'monserat',
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Hi, " + "$name",
                        style: TextStyle(
                          fontSize: 16.0,
                          fontFamily: 'monserat',
                          color: Colors.black,
                        ),
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.location_on,
                            color: BaseURL.colorGlobal,
                            size: 14,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "$nama_region",
                            style: TextStyle(
                              fontSize: 13.0,
                              fontFamily: 'monserat',
                              color: Colors.grey[800],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              InkWell(
                onTap: () {
                  _keluarAplikasi();
                },
                child: Icon(
                  Icons.exit_to_app,
                  size: 30,
                  color: BaseURL.colorGlobal,
                ),
              ),
            ],
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(15.0),
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 5, right: 5, top: 5),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 5,
                        offset: Offset(0, 2),
                      ),
                    ],
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      bottomLeft: Radius.circular(20.0),
                      bottomRight: Radius.circular(20.0),
                      topRight: Radius.circular(20.0),
                    ),
                  ),
                  child: FormField<String>(
                    builder: (FormFieldState<String> state) {
                      return InputDecorator(
                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            isExpanded: true,
                            hint: Text(
                              "Semua Sekolah",
                              style: TextStyle(
                                fontFamily: 'monserat',
                              ),
                            ),
                            value: idsekolah,
                            isDense: true,
                            onChanged: (newValue) {
                              setState(() {
                                idsekolah = newValue;
                                statusgrafik = idsekolah;
                              });
                              if (idsekolah != "0") {
                                _datagrafikSekolah();
                              }
                            },
                            items: _listRegional.map((ModalRegional value) {
                              return DropdownMenuItem<String>(
                                value: value.id_sekolah,
                                child: Text(
                                  value.nama_sekolah,
                                  maxLines: 1,
                                  style: TextStyle(
                                    fontFamily: 'monserat',
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            Future.delayed(Duration(seconds: 1), () {
                              Navigator.of(context).pop(true);
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      ListAdminSekolah(id_region)));
                            });
                            return AlertDialog(
                              title: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  new CircularProgressIndicator(),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  new Text(
                                    "Mengambil data ...",
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontFamily: 'monserat',
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width / 2,
                        child: Column(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20.0),
                                  bottomLeft: Radius.circular(20.0),
                                  bottomRight: Radius.circular(20.0),
                                  topRight: Radius.circular(20.0),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 7,
                                    offset: Offset(0, 2),
                                  ),
                                ],
                              ),
                              width: 150,
                              height: 100,
                              child: Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Image(
                                      image: AssetImage(
                                          "assets/img/btn-adminsekolah.png"),
                                      height: 40,
                                      width: 40,
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Center(
                                      child: Text(
                                        'Data Operator',
                                        style: TextStyle(
                                          fontFamily: 'monserat',
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    Center(
                                      child: Text(
                                        'Sekolah',
                                        style: TextStyle(
                                          fontFamily: 'monserat',
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            Future.delayed(Duration(seconds: 1), () {
                              Navigator.of(context).pop(true);
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      UbahPassword(username, roleid, signOut)));
                            });
                            return AlertDialog(
                              title: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  new CircularProgressIndicator(),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  new Text(
                                    "Mengambil data ...",
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontFamily: 'monserat',
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width / 2,
                        child: Column(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20.0),
                                  bottomLeft: Radius.circular(20.0),
                                  bottomRight: Radius.circular(20.0),
                                  topRight: Radius.circular(20.0),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 7,
                                    offset: Offset(0, 2),
                                  ),
                                ],
                              ),
                              width: 150,
                              height: 100,
                              child: Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Image(
                                      image: AssetImage(
                                          "assets/img/btn-password.png"),
                                      height: 40,
                                      width: 40,
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Center(
                                      child: Text(
                                        'Ubah',
                                        style: TextStyle(
                                          fontFamily: 'monserat',
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    Center(
                                      child: Text(
                                        'Kata Sandi',
                                        style: TextStyle(
                                          fontFamily: 'monserat',
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              statusgrafik == "0" ? semuasekolah() : datapersekolah(),
            ],
          ),
        ),
      ),
    );
  }
}

class ClicksPerYear {
  final String year;
  final int clicks;
  final charts.Color color;

  ClicksPerYear(this.year, this.clicks, Color color)
      : this.color = charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}
