import 'dart:convert';
import 'dart:io';

import 'package:apps_zimbraadmin/modal/api.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;

class DialogUpdateOperator extends StatefulWidget {
  final String nuptk;
  final VoidCallback reload;
  final VoidCallback infobenar;
  final VoidCallback infosalah;
  DialogUpdateOperator(this.nuptk, this.reload, this.infobenar, this.infosalah);
  @override
  _DialogUpdateOperatorState createState() => _DialogUpdateOperatorState();
}

class _DialogUpdateOperatorState extends State<DialogUpdateOperator> {
  var loading = false;
  TextEditingController datanuptk = new TextEditingController();
  TextEditingController asalsekolah = new TextEditingController();
  TextEditingController nama = new TextEditingController();
  TextEditingController nomorhp = new TextEditingController();
  TextEditingController email = new TextEditingController();

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  _tampildata(String nuptk) async {
    setState(() {
      loading = true;
    });
    final response = await http.get(
      BaseURL.getinfo_sekolah + nuptk,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + BaseURL.token,
      },
    );
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      final result = data['data'][0];

      datanuptk = TextEditingController(text: result['nuptk']);
      asalsekolah = TextEditingController(text: result['sekolah']);
      nama = TextEditingController(text: result['nama']);
      nomorhp = TextEditingController(text: result['no_hp']);
      email = TextEditingController(text: result['email']);
    }
    setState(() {
      loading = true;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tampildata(widget.nuptk);
  }

  _updatedataoperator(
      String nuptk, String nama, String email, String nohp) async {
    final response = await http.post(
      BaseURL.editoptr_sekolah,
      body: {
        'nuptk': nuptk,
        'nama': nama,
        'email': email,
        'no_hp': nohp,
      },
      headers: {
        HttpHeaders.authorizationHeader: "Bearer " + BaseURL.token,
      },
    );
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      final value = data['value'];
      if (value == 1) {
        widget.infobenar();
        widget.reload();
      } else {
        widget.infosalah();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: [
          Column(
            children: [
              AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0))),
                title: Text(
                  'Ubah Data Operator',
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                  ),
                ),
                content: Column(children: [
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: new BorderRadius.circular(15.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(1),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 3, bottom: 3, left: 10, right: 10),
                          child: TextFormField(
                            readOnly: true,
                            controller: datanuptk,
                            style: new TextStyle(
                              fontFamily: 'monserat',
                            ),
                            keyboardType: TextInputType.text,
                            autofocus: false,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "NUPTK",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: new BorderRadius.circular(15.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(1),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 3, bottom: 3, left: 10, right: 10),
                          child: TextFormField(
                            readOnly: true,
                            controller: asalsekolah,
                            style: new TextStyle(
                              fontFamily: 'monserat',
                            ),
                            keyboardType: TextInputType.text,
                            autofocus: false,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Asal Sekolah",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.circular(15.0),
                        border: Border.all(
                          color: Colors.black,
                          width: 1,
                        ),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(1),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 3, bottom: 3, left: 10, right: 10),
                          child: TextFormField(
                            controller: nama,
                            style: new TextStyle(
                              fontFamily: 'monserat',
                            ),
                            keyboardType: TextInputType.text,
                            autofocus: false,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Nama",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.circular(15.0),
                        border: Border.all(
                          color: Colors.black,
                          width: 1,
                        ),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(1),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 3, bottom: 3, left: 10, right: 10),
                          child: TextFormField(
                            controller: nomorhp,
                            style: new TextStyle(
                              fontFamily: 'monserat',
                            ),
                            keyboardType: TextInputType.text,
                            autofocus: false,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Nomor Hp",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.circular(15.0),
                        border: Border.all(
                          color: Colors.black,
                          width: 1,
                        ),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(1),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 3, bottom: 3, left: 10, right: 10),
                          child: TextFormField(
                            controller: email,
                            style: new TextStyle(
                              fontFamily: 'monserat',
                            ),
                            keyboardType: TextInputType.text,
                            autofocus: false,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Email",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Batal",
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: 'monserat',
                            ),
                          )),
                      SizedBox(
                        width: 16,
                      ),
                      InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            showDialog(
                              context: context,
                              builder: (context) {
                                Future.delayed(Duration(seconds: 1), () {
                                  // Navigator.of(context).pop(true);
                                  Navigator.pop(context);
                                  _updatedataoperator(datanuptk.text, nama.text,
                                      email.text, nomorhp.text);
                                });
                                return AlertDialog(
                                  title: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      new CircularProgressIndicator(),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      new Text(
                                        "Proses ...",
                                        style: TextStyle(
                                          fontSize: 15,
                                          fontFamily: 'monserat',
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            );
                          },
                          child: Text(
                            "Perbarui",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              fontFamily: 'monserat',
                              color: BaseURL.colorGlobal,
                            ),
                          )),
                    ],
                  )
                ]),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
