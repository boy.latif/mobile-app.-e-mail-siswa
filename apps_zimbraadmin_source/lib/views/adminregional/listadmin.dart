import 'dart:convert';
import 'dart:io';

import 'package:apps_zimbraadmin/modal/api.dart';
import 'package:apps_zimbraadmin/modal/modalDataAdminSiswa.dart';
import 'package:apps_zimbraadmin/views/adminregional/dialogupdate_regional.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class ListAdminSekolah extends StatefulWidget {
  final String idregional;
  ListAdminSekolah(this.idregional);
  @override
  _ListAdminSekolahState createState() => _ListAdminSekolahState();
}

class _ListAdminSekolahState extends State<ListAdminSekolah> {
  List<ModalDataAdminSiswa> _list = [];
  List<ModalDataAdminSiswa> _search = [];
  var loading = false;
  final GlobalKey<RefreshIndicatorState> _refresh =
      GlobalKey<RefreshIndicatorState>();

  Future<List<ModalDataAdminSiswa>> _getdataAdminSekolah() async {
    _list.clear();
    setState(() {
      loading = true;
    });
    final response = await http.get(
      BaseURL.getDataAdminRegional + widget.idregional,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + BaseURL.token,
      },
    );
    if (response.contentLength == 2) {
      setState(() {
        loading = false;
      });
    } else {
      final data = jsonDecode(response.body);
      final result = data['data'];
      result.forEach((api) {
        final ab = new ModalDataAdminSiswa(
          api['id'],
          api['nuptk'],
          api['nik'],
          api['sekolah'],
          api['npsn'],
          api['nama'],
          api['tgl_lahir'],
          api['email'],
          api['no_hp'],
          api['password'],
          api['statuss'],
        );
        _list.add(ab);
      });
      _search = _list;
      setState(() {
        loading = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _getdataAdminSekolah();
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  prosesdata(String status, String id) async {
    final response = await http.post(
      BaseURL.ubahStatus,
      body: {
        'status': status,
        'id': id,
      },
      headers: {
        HttpHeaders.authorizationHeader: "Bearer " + BaseURL.token,
      },
    );
    final data = jsonDecode(response.body);
    int value = data['value'];
    if (value == 1) {
      String status = data['status'];
      _getdataAdminSekolah();
      if (status == "0") {
        showToast("Selamat, Data berhasil dinonaktifkan.",
            gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      } else {
        showToast("Selamat, Data berhasil diaktifkan.",
            gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      }
    } else {
      showToast("Maaf, Data anda gagal diproses.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  status(String status, id) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(32.0))),
          contentPadding: EdgeInsets.only(top: 10.0),
          content: Container(
            width: 400.0,
            height: 250,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: 100,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/img/confirm_check.png'),
                      fit: BoxFit.fitHeight,
                    ),
                    shape: BoxShape.circle,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: ListView(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Center(
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text(
                                status == "1"
                                    ? "Apakah anda yakin, akan menonaktifkan akun tersebut?"
                                    : "Apakah anda yakin, akan mengaktifkan akun tersebut?",
                                style: TextStyle(
                                  fontFamily: 'monserat',
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  decoration: BoxDecoration(
                    color: BaseURL.colorGlobal,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(32.0),
                      bottomRight: Radius.circular(32.0),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          showDialog(
                            context: context,
                            builder: (context) {
                              Future.delayed(Duration(seconds: 1), () {
                                Navigator.of(context).pop(true);
                                prosesdata(status == "1" ? "0" : "1", id);
                              });
                              return AlertDialog(
                                title: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    new CircularProgressIndicator(),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    new Text(
                                      "Proses ...",
                                      style: TextStyle(
                                        fontSize: 15,
                                        fontFamily: 'monserat',
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          );
                        },
                        child: Text(
                          status == "1" ? "NON AKTIFKAN" : "AKTIFKAN",
                          style: TextStyle(
                            color: status == "1"
                                ? Colors.red[300]
                                : Colors.green[300],
                            fontFamily: 'monserat',
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          "TIDAK",
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'monserat',
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  _hapusDataOperator(String nuptk) async {
    final response = await http.post(
      BaseURL.deleteoptr_sekolah,
      body: {
        'nuptk': nuptk,
      },
      headers: {
        HttpHeaders.authorizationHeader: "Bearer " + BaseURL.token,
      },
    );
    final data = jsonDecode(response.body);
    int value = data['value'];
    String pesan = data['message'];
    if (value == 1) {
      _getdataAdminSekolah();
      showToast(pesan, gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else {
      showToast(pesan, gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
  }

  pesansalah() {
    showToast("Data tidak ada yang di ubah atau nuptk tidak ada!",
        gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
  }

  pesanbenar() {
    showToast("Data berhasil diupdate!",
        gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
  }

  _setelData(String nuptk, String statusaktif, String statusid) {
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          child: ListView(
            padding: EdgeInsets.all(16),
            shrinkWrap: true,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      // _ubahdataSiswa(nisn);
                      showDialog(
                          context: context,
                          builder: (_) {
                            return DialogUpdateOperator(nuptk,
                                _getdataAdminSekolah, pesanbenar, pesansalah);
                          });
                    },
                    child: Column(
                      children: [
                        Icon(
                          Icons.edit,
                          color: BaseURL.colorGlobal,
                        ),
                        Text(
                          "Ubah",
                          style: TextStyle(
                            fontFamily: 'monserat',
                            color: BaseURL.colorGlobal,
                          ),
                        ),
                        Text(
                          "Data",
                          style: TextStyle(
                            fontFamily: 'monserat',
                            color: BaseURL.colorGlobal,
                          ),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      showDialog(
                        context: context,
                        builder: (context) {
                          Future.delayed(Duration(seconds: 1), () {
                            Navigator.of(context).pop(true);
                            _hapusDataOperator(nuptk);
                          });
                          return AlertDialog(
                            title: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                new CircularProgressIndicator(),
                                SizedBox(
                                  width: 10,
                                ),
                                new Text(
                                  "Proses ...",
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontFamily: 'monserat',
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      );
                    },
                    child: Column(
                      children: [
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        Text(
                          "Hapus",
                          style: TextStyle(
                            fontFamily: 'monserat',
                            color: Colors.red,
                          ),
                        ),
                        Text(
                          "Data",
                          style: TextStyle(
                            fontFamily: 'monserat',
                            color: Colors.red,
                          ),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      status(statusaktif, statusid);
                    },
                    child: Column(
                      children: [
                        Icon(
                          Icons.lock,
                          color: statusaktif == "1" ? Colors.red : Colors.green,
                        ),
                        Text(
                          statusaktif == "1" ? "Non Aktif" : "Aktif",
                          style: TextStyle(
                            fontFamily: 'monserat',
                            color:
                                statusaktif == "1" ? Colors.red : Colors.green,
                          ),
                        ),
                        Text(
                          "User",
                          style: TextStyle(
                            fontFamily: 'monserat',
                            color:
                                statusaktif == "1" ? Colors.red : Colors.green,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }

  _listitem(x) {
    String haslnik;
    if (_search[x].nik == null) {
      haslnik = "";
    } else {
      haslnik = _search[x].nik;
    }
    return Card(
      color: Colors.transparent,
      elevation: 0,
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(
              color: BaseURL.focusColorTextField,
              width: 5.0,
            ),
          ),
        ),
        child: Container(
          decoration: BoxDecoration(
            // color: Colors.white,
            color: _search[x].statuss == "1" ? Colors.white : Colors.grey[300],
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(0.0),
              bottomLeft: Radius.circular(0.0),
              bottomRight: Radius.circular(5.0),
              topRight: Radius.circular(5.0),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 1,
                offset: Offset(0, 1),
              ),
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        _search[x].sekolah,
                        maxLines: 1,
                        style: TextStyle(
                          fontFamily: 'monserat',
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        _search[x].nuptk + "/" + haslnik,
                        maxLines: 1,
                        style: TextStyle(
                          fontFamily: 'monserat',
                          fontSize: 12,
                        ),
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.person,
                            size: 12,
                            color: BaseURL.colorGlobal,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            _search[x].nama,
                            style: TextStyle(
                              fontFamily: 'monserat',
                              fontSize: 12,
                            ),
                          ),
                          Text(
                            " | ",
                            style: TextStyle(
                              fontFamily: 'monserat',
                              fontSize: 12,
                            ),
                          ),
                          Icon(
                            Icons.mail,
                            size: 12,
                            color: BaseURL.colorGlobal,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Expanded(
                            child: Text(
                              _search[x].email,
                              maxLines: 1,
                              style: TextStyle(
                                fontFamily: 'monserat',
                                fontSize: 12,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: BaseURL.colorGlobal,
                    borderRadius: BorderRadius.all(
                        Radius.circular(20.0) //         <--- border radius here
                        ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: InkWell(
                      onTap: () {
                        _setelData(_search[x].nuptk, _search[x].statuss,
                            _search[x].id);
                      },
                      child: Row(
                        children: [
                          Image(
                            height: 15,
                            width: 15,
                            image: AssetImage("assets/img/hand.png"),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Pilih",
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _searchBar() {
    return TextField(
      // controller: controller,
      // onChanged: onSearch,
      decoration: new InputDecoration(
        hintText: "Cari ...",
        hintStyle: TextStyle(
          fontFamily: 'monserat',
          fontSize: 15,
        ),
        fillColor: Colors.white,
        border: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(50.0),
          borderSide: new BorderSide(),
        ),
        suffixIcon: IconButton(
          icon: Icon(
            Icons.search,
          ),
          onPressed: () {
            // _mencaridata(ktp.text);
          },
        ),
      ),
      keyboardType: TextInputType.text,
      style: new TextStyle(
        fontFamily: 'monserat',
        fontSize: 15,
      ),
      onChanged: (text) {
        text = text.toLowerCase();
        setState(() {
          _search = _list.where((note) {
            var noteTitle = note.sekolah.toLowerCase();
            return noteTitle.contains(text);
          }).toList();
        });
      },
    );
  }

  String statusbar = "0";
  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.black,
      ));
      statusbar = "0";
    } else if (Platform.isIOS) {
      // iOS-specific code
      statusbar = "1";
    }
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: BaseURL.colorGlobal,
      ),
      home: Scaffold(
        appBar: AppBar(
          brightness: statusbar == "1" ? Brightness.light : Brightness.dark,
          backgroundColor: Colors.white,
          toolbarHeight: 60,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      height: 25,
                      width: 25,
                      decoration: BoxDecoration(
                        color: BaseURL.colorGlobal,
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.black54,
                              blurRadius: 3.0,
                              offset: Offset(0.5, 1))
                        ],
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                      child: Center(
                        child: Icon(
                          Icons.arrow_back,
                          size: 18,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Data Operator Sekolah",
                            style: TextStyle(
                              fontSize: 18.0,
                              fontFamily: 'monserat',
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        body: RefreshIndicator(
          onRefresh: _getdataAdminSekolah,
          key: _refresh,
          child: loading || _list.length == 0
              ? ListView.builder(
                  itemCount: 1,
                  itemBuilder: (ctx, i) {
                    return Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 100),
                          child: Center(
                            child: new Container(
                                width: 120.00,
                                height: 100.00,
                                decoration: new BoxDecoration(
                                  image: new DecorationImage(
                                    image: ExactAssetImage(
                                        'assets/img/nodata.png'),
                                    fit: BoxFit.fitHeight,
                                  ),
                                )),
                          ),
                        ),
                        Text(
                          "Data Kosong",
                          style: TextStyle(
                            fontFamily: 'monserat',
                          ),
                        ),
                      ],
                    );
                  })
              : Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // SizedBox(
                      //   height: 5,
                      // ),
                      Expanded(
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            // final x = _list[i];
                            return index == 0
                                ? _searchBar()
                                : _listitem(index - 1);
                          },
                          itemCount: _search.length + 1,
                        ),
                      )
                    ],
                  ),
                ),
        ),
      ),
    );
  }
}
