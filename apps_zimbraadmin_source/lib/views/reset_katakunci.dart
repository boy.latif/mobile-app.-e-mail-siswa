import 'dart:convert';
import 'dart:io';

import 'package:apps_zimbraadmin/modal/api.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;

class ResetKataKunci extends StatefulWidget {
  @override
  _ResetKataKunciState createState() => _ResetKataKunciState();
}

class _ResetKataKunciState extends State<ResetKataKunci> {
  TextEditingController reset_key = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController nuptk = new TextEditingController();
  String statusbar = "0";

  _proses() {
    showDialog(
      context: context,
      builder: (context) {
        Future.delayed(Duration(seconds: 1), () {
          Navigator.of(context).pop(true);
          _check();
        });
        return AlertDialog(
          title: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              new CircularProgressIndicator(),
              SizedBox(
                width: 10,
              ),
              new Text(
                "Proses ...",
                style: TextStyle(
                  fontSize: 15,
                  fontFamily: 'monserat',
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  _check() async {
    if (reset_key.text == "") {
      showToast("Maaf, key tidak boleh kosong.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else if (password.text == "") {
      showToast("Maaf, kata sandi tidak boleh kosong.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else if (nuptk.text == "") {
      showToast("Maaf, nuptk tidak boleh kosong.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    } else {
      final response = await http.post(
        BaseURL.resetkey,
        body: {
          'reset_key': reset_key.text,
          'password': password.text,
          'nuptk': nuptk.text,
        },
        headers: {
          HttpHeaders.authorizationHeader: "Bearer " + BaseURL.token,
        },
      );
      final data = jsonDecode(response.body);
      if (data['value'] == 0) {
        showToast(data['message'],
            gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      } else {
        final message = data['message'];
        _hasilinfo(message);
      }
    }
  }

  _hasilinfo(String pesan) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(32.0))),
          contentPadding: EdgeInsets.only(top: 10.0),
          content: Container(
            width: 400.0,
            height: 300,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: 100,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/img/confirm_check.png'),
                      fit: BoxFit.fitHeight,
                    ),
                    shape: BoxShape.circle,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: ListView(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Center(
                        child: Column(
                          children: [
                            Text(
                              "Data Berhasil Disimpan.",
                              style: TextStyle(
                                fontFamily: 'monserat',
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              pesan,
                              style: TextStyle(
                                fontFamily: 'monserat',
                                fontSize: 14,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  decoration: BoxDecoration(
                    color: BaseURL.colorGlobal,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(32.0),
                      bottomRight: Radius.circular(32.0),
                    ),
                  ),
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pop(context);
                    },
                    child: Text(
                      "TUTUP",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'monserat',
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.black,
      ));
      statusbar = "0";
    } else if (Platform.isIOS) {
      // iOS-specific code
      statusbar = "1";
    }
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/img/bg.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: <Widget>[
            Container(
              child: Center(
                child: SingleChildScrollView(
                  child: Stack(
                    children: <Widget>[
                      ListView(
                        shrinkWrap: true,
                        padding: EdgeInsets.all(10),
                        children: <Widget>[
                          Column(
                            children: [
                              Text(
                                "Reset Kata Kunci",
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: 'monserat',
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                left: 15,
                                right: 15,
                              ),
                              child: Card(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30.0)),
                                elevation: 8.0,
                                child: Container(
                                  padding: EdgeInsets.all(20.0),
                                  child: Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            color: Colors.grey[200],
                                            borderRadius:
                                                new BorderRadius.circular(15.0),
                                          ),
                                          child: Padding(
                                            padding: EdgeInsets.all(1),
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 3,
                                                  bottom: 3,
                                                  left: 10,
                                                  right: 10),
                                              child: TextFormField(
                                                controller: reset_key,
                                                keyboardType:
                                                    TextInputType.number,
                                                style: new TextStyle(
                                                  fontFamily: 'monserat',
                                                ),
                                                autofocus: false,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  prefixIcon:
                                                      Icon(Icons.vpn_key),
                                                  hintText: "Key Data",
                                                  hintStyle: TextStyle(
                                                    fontFamily: 'monserat',
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            color: Colors.grey[200],
                                            borderRadius:
                                                new BorderRadius.circular(15.0),
                                          ),
                                          child: Padding(
                                            padding: EdgeInsets.all(1),
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 3,
                                                  bottom: 3,
                                                  left: 10,
                                                  right: 10),
                                              child: TextFormField(
                                                controller: password,
                                                keyboardType:
                                                    TextInputType.number,
                                                style: new TextStyle(
                                                  fontFamily: 'monserat',
                                                ),
                                                autofocus: false,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  prefixIcon:
                                                      Icon(Icons.lock_open),
                                                  hintText: "Kata Sandi",
                                                  hintStyle: TextStyle(
                                                    fontFamily: 'monserat',
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(top: 10),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            color: Colors.grey[200],
                                            borderRadius:
                                                new BorderRadius.circular(15.0),
                                          ),
                                          child: Padding(
                                            padding: EdgeInsets.all(1),
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 3,
                                                  bottom: 3,
                                                  left: 10,
                                                  right: 10),
                                              child: TextFormField(
                                                controller: nuptk,
                                                keyboardType:
                                                    TextInputType.number,
                                                style: new TextStyle(
                                                  fontFamily: 'monserat',
                                                ),
                                                autofocus: false,
                                                decoration: InputDecoration(
                                                  border: InputBorder.none,
                                                  prefixIcon:
                                                      Icon(Icons.credit_card),
                                                  hintText: "NUPTK",
                                                  hintStyle: TextStyle(
                                                    fontFamily: 'monserat',
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        height: 15.0,
                                      ),
                                      Column(
                                        children: <Widget>[
                                          Material(
                                            child: MaterialButton(
                                              shape: StadiumBorder(),
                                              onPressed: () => {
                                                _proses(),
                                              },
                                              minWidth: 350.0,
                                              height: 50.0,
                                              color:
                                                  BaseURL.focusColorTextField,
                                              child: Text(
                                                "RESET",
                                                style: TextStyle(
                                                  fontSize: 16.0,
                                                  fontFamily: 'monserat',
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Row(
                                            children: [
                                              Text(
                                                "Batal ?",
                                                style: TextStyle(
                                                  fontFamily: 'monserat',
                                                ),
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Text(
                                                  " Klik disini!",
                                                  style: TextStyle(
                                                    fontFamily: 'monserat',
                                                    fontWeight: FontWeight.bold,
                                                    color: BaseURL.colorGlobal,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "2020 © KEMENDIKBUD - KOMINFO",
                                textScaleFactor: 1,
                                style: TextStyle(
                                  fontSize: 15,
                                  fontFamily: 'monserat',
                                ),
                              ),
                              Text(
                                "All rights Reserved",
                                textScaleFactor: 1,
                                style: TextStyle(
                                  fontSize: 15,
                                  fontFamily: 'monserat',
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
