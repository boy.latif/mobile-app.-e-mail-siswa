import 'dart:convert';
import 'dart:io';

import 'package:apps_zimbraadmin/modal/api.dart';
import 'package:apps_zimbraadmin/views/adminsekolah/daftar_email_siswa.dart';
import 'package:apps_zimbraadmin/views/adminsekolah/data_siswa.dart';
import 'package:apps_zimbraadmin/views/ubahpassword.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class BerandaAdminSekolah extends StatefulWidget {
  final VoidCallback signOut;
  BerandaAdminSekolah(this.signOut);
  @override
  _BerandaAdminSekolahState createState() => _BerandaAdminSekolahState();
}

class _BerandaAdminSekolahState extends State<BerandaAdminSekolah> {
  var jenisKelamin;
  String username,
      roleid,
      nik,
      sekolah,
      npsn,
      nama,
      tgl_lahir,
      email,
      no_hp,
      id_region,
      nama_region,
      nama_awal,
      id_sekolah;
  int sudah = 0, belum = 0, laki_laki = 0, perempuan = 0;

  signOut() {
    setState(() {
      widget.signOut();
    });
  }

  _keluarAplikasi() {
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          child: ListView(
            padding: EdgeInsets.all(16),
            shrinkWrap: true,
            children: <Widget>[
              Text(
                "Keluar dari Aplikasi Email Siswa Nasional?",
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'monserat',
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Apakah anda yakin ingin keluar?",
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'monserat',
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Tidak",
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'monserat',
                        ),
                      )),
                  SizedBox(
                    width: 16,
                  ),
                  InkWell(
                      onTap: () {
                        Navigator.pop(context);
                        signOut();
                      },
                      child: Text(
                        "Keluar",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          fontFamily: 'monserat',
                          color: BaseURL.colorGlobal,
                        ),
                      )),
                ],
              )
            ],
          ),
        );
      },
    );
  }

  getgrafik_siswaberdasarkansekolah() async {
    final response = await http.get(
      BaseURL.getinfo_grafikberdasarkansekolah + id_sekolah,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + BaseURL.token,
      },
    );
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        sudah = data['sudah'];
        belum = data['belum'];
        laki_laki = data['laki'];
        perempuan = data['perempuan'];
      });
    }
  }

  getinfo_sekolah() async {
    final response = await http.get(
      BaseURL.getinfo_sekolah + username,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + BaseURL.token,
      },
    );
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      setState(() {
        nik = data['data'][0]['nik'];
        sekolah = data['data'][0]['sekolah'];
        npsn = data['data'][0]['npsn'];
        nama = data['data'][0]['nama'];
        tgl_lahir = data['data'][0]['tgl_lahir'];
        email = data['data'][0]['email'];
        no_hp = data['data'][0]['no_hp'];
        id_region = data['data'][0]['id_region'];
        nama_region = data['nama_region'];
        nama_awal = data['nama_awal'][0];
        id_sekolah = data['data'][0]['id_sekolah'];
      });
      getgrafik_siswaberdasarkansekolah();
    }
  }

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      roleid = preferences.getString('roleid');
      username = preferences.getString('username');
    });
    getinfo_sekolah();
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  String statusbar = "0";

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.black,
      ));
      statusbar = "0";
    } else if (Platform.isIOS) {
      // iOS-specific code
      statusbar = "1";
    }

    // pendaftar email berdasarkan sekolah
    var data = [
      ClicksPerYear('Sudah', sudah, BaseURL.colorGlobal),
      ClicksPerYear('Belum', belum, BaseURL.colorGlobal),
    ];

    var series = [
      charts.Series(
          domainFn: (ClicksPerYear clickData, _) => clickData.year,
          measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
          colorFn: (ClicksPerYear clickData, _) => clickData.color,
          id: 'id',
          data: data,
          labelAccessorFn: (ClicksPerYear clickData, _) =>
              '${clickData.clicks.toString()}'),
    ];

    var chart = charts.BarChart(
      series,
      animate: true,
      vertical: true,
      barRendererDecorator: new charts.BarLabelDecorator<String>(),
    );

    var chartWidget = Padding(
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
        top: 20,
        bottom: 20,
      ),
      child: SizedBox(
        height: 200.0,
        child: chart,
      ),
    );

    // pendaftar berdasarkan jenjang
    var data2 = [
      ClicksPerYear('Laki-laki', laki_laki, BaseURL.colorGlobal),
      ClicksPerYear('Perempuan', perempuan, BaseURL.colorGlobal),
    ];

    var series2 = [
      charts.Series(
          domainFn: (ClicksPerYear clickData, _) => clickData.year,
          measureFn: (ClicksPerYear clickData, _) => clickData.clicks,
          colorFn: (ClicksPerYear clickData, _) => clickData.color,
          id: 'id',
          data: data2,
          labelAccessorFn: (ClicksPerYear clickData, _) =>
              '${clickData.clicks.toString()}'),
    ];

    var chart2 = charts.BarChart(
      series2,
      animate: true,
      vertical: true,
      barRendererDecorator: new charts.BarLabelDecorator<String>(),
    );

    var chartWidget2 = Padding(
      padding: EdgeInsets.only(
        left: 20,
        right: 20,
        top: 20,
        bottom: 20,
      ),
      child: SizedBox(
        height: 200.0,
        child: chart2,
      ),
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          brightness: statusbar == "1" ? Brightness.light : Brightness.dark,
          backgroundColor: Colors.white,
          toolbarHeight: 80,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                      color: BaseURL.colorGlobal,
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.black54,
                            blurRadius: 3.0,
                            offset: Offset(0.5, 1))
                      ],
                      borderRadius: BorderRadius.circular(50.0),
                    ),
                    child: Center(
                      child: Text(
                        "$nama_awal",
                        style: TextStyle(
                          fontSize: 20.0,
                          fontFamily: 'monserat',
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Hi, " + "$nama",
                        style: TextStyle(
                          fontSize: 16.0,
                          fontFamily: 'monserat',
                          color: Colors.black,
                        ),
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.school,
                            color: BaseURL.colorGlobal,
                            size: 14,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "$sekolah",
                            style: TextStyle(
                              fontSize: 13.0,
                              fontFamily: 'monserat',
                              color: Colors.grey[800],
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.location_on,
                            color: BaseURL.colorGlobal,
                            size: 14,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "$nama_region",
                            style: TextStyle(
                              fontSize: 13.0,
                              fontFamily: 'monserat',
                              color: Colors.grey[800],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              InkWell(
                onTap: () {
                  _keluarAplikasi();
                },
                child: Icon(
                  Icons.exit_to_app,
                  size: 30,
                  color: BaseURL.colorGlobal,
                ),
              ),
            ],
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(15.0),
          child: ListView(
            children: [
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            Future.delayed(Duration(seconds: 1), () {
                              Navigator.of(context).pop(true);
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      DataSiswaAdminSekolah(id_sekolah)));
                            });
                            return AlertDialog(
                              title: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  new CircularProgressIndicator(),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  new Text(
                                    "Mengambil data ...",
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontFamily: 'monserat',
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width / 3,
                        child: Column(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20.0),
                                  bottomLeft: Radius.circular(20.0),
                                  bottomRight: Radius.circular(20.0),
                                  topRight: Radius.circular(20.0),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 7,
                                    offset: Offset(0, 2),
                                  ),
                                ],
                              ),
                              width: 100,
                              height: 100,
                              child: Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Image(
                                      image: AssetImage(
                                          "assets/img/btn-siswa.png"),
                                      height: 40,
                                      width: 40,
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Center(
                                      child: Text(
                                        'Data',
                                        style: TextStyle(
                                          fontFamily: 'monserat',
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    Center(
                                      child: Text(
                                        'Siswa',
                                        style: TextStyle(
                                          fontFamily: 'monserat',
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            Future.delayed(Duration(seconds: 1), () {
                              Navigator.of(context).pop(true);
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      DaftarEmailSiswaAdminSekolah(
                                          getgrafik_siswaberdasarkansekolah,
                                          id_sekolah)));
                            });
                            return AlertDialog(
                              title: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  new CircularProgressIndicator(),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  new Text(
                                    "Mengambil data ...",
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontFamily: 'monserat',
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width / 3,
                        child: Column(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20.0),
                                  bottomLeft: Radius.circular(20.0),
                                  bottomRight: Radius.circular(20.0),
                                  topRight: Radius.circular(20.0),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 7,
                                    offset: Offset(0, 2),
                                  ),
                                ],
                              ),
                              width: 100,
                              height: 100,
                              child: Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Image(
                                      image: AssetImage(
                                          "assets/img/btn-daftaremail.png"),
                                      height: 40,
                                      width: 40,
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Center(
                                      child: Text(
                                        'Daftar Email',
                                        style: TextStyle(
                                          fontFamily: 'monserat',
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    Center(
                                      child: Text(
                                        'Siswa',
                                        style: TextStyle(
                                          fontFamily: 'monserat',
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            Future.delayed(Duration(seconds: 1), () {
                              Navigator.of(context).pop(true);
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      UbahPassword(username, roleid, signOut)));
                            });
                            return AlertDialog(
                              title: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  new CircularProgressIndicator(),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  new Text(
                                    "Mengambil data ...",
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontFamily: 'monserat',
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width / 3,
                        child: Column(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20.0),
                                  bottomLeft: Radius.circular(20.0),
                                  bottomRight: Radius.circular(20.0),
                                  topRight: Radius.circular(20.0),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 7,
                                    offset: Offset(0, 2),
                                  ),
                                ],
                              ),
                              width: 100,
                              height: 100,
                              child: Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Image(
                                      image: AssetImage(
                                          "assets/img/btn-ubahpass.png"),
                                      height: 40,
                                      width: 40,
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Center(
                                      child: Text(
                                        'Ubah',
                                        style: TextStyle(
                                          fontFamily: 'monserat',
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    Center(
                                      child: Text(
                                        'Kata Sandi',
                                        style: TextStyle(
                                          fontFamily: 'monserat',
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Text(
                  "Email Siswa Berdasarkan Sekolah",
                  style: TextStyle(
                    // fontSize: 12,
                    color: Colors.grey[700],
                    fontFamily: 'monserat',
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      bottomLeft: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0),
                      topRight: Radius.circular(10.0),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 5,
                        offset: Offset(0, 2),
                      ),
                    ],
                  ),
                  child: chartWidget,
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 5),
                child: Text(
                  "Email Siswa Berdasarkan Jenis Kelamin",
                  style: TextStyle(
                    // fontSize: 12,
                    color: Colors.grey[700],
                    fontFamily: 'monserat',
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      bottomLeft: Radius.circular(10.0),
                      bottomRight: Radius.circular(10.0),
                      topRight: Radius.circular(10.0),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 5,
                        offset: Offset(0, 2),
                      ),
                    ],
                  ),
                  child: chartWidget2,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ClicksPerYear {
  final String year;
  final int clicks;
  final charts.Color color;

  ClicksPerYear(this.year, this.clicks, Color color)
      : this.color = charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}
