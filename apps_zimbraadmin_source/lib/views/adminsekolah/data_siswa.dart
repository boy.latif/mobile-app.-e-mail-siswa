import 'dart:convert';
import 'dart:io';

import 'package:apps_zimbraadmin/modal/api.dart';
import 'package:apps_zimbraadmin/modal/modalDataSiswa.dart';
import 'package:apps_zimbraadmin/views/adminsekolah/dialogupdate_siswa.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class DataSiswaAdminSekolah extends StatefulWidget {
  final String idsekolah;
  DataSiswaAdminSekolah(this.idsekolah);
  @override
  _DataSiswaAdminSekolahState createState() => _DataSiswaAdminSekolahState();
}

class _DataSiswaAdminSekolahState extends State<DataSiswaAdminSekolah> {
  String statusbar = "0";
  var loading = false;
  // List<ModalDataSiswa> list = new List<ModalDataSiswa>();
  // List<ModalDataSiswa> _listForDisplay = List<ModalDataSiswa>();
  List<ModalDataSiswa> _list = [];
  List<ModalDataSiswa> _search = [];
  final GlobalKey<RefreshIndicatorState> _refresh =
      GlobalKey<RefreshIndicatorState>();

  TextEditingController idsiswa = new TextEditingController();
  TextEditingController nisn = new TextEditingController();
  TextEditingController npsn = new TextEditingController();
  TextEditingController nik = new TextEditingController();
  TextEditingController nama = new TextEditingController();
  TextEditingController alamat = new TextEditingController();
  TextEditingController sex = new TextEditingController();
  TextEditingController tgl_lahir = new TextEditingController();
  TextEditingController email = new TextEditingController();
  TextEditingController nohp = new TextEditingController();
  TextEditingController email_alter = new TextEditingController();
  TextEditingController nama_orangtua = new TextEditingController();
  TextEditingController no_orangtua = new TextEditingController();
  TextEditingController email_orangtua = new TextEditingController();
  TextEditingController nm_sekolah = new TextEditingController();
  TextEditingController nama_region = new TextEditingController();
  TextEditingController xcreate_date = new TextEditingController();
  TextEditingController _textController = TextEditingController();

  Future<List<ModalDataSiswa>> _getdata_siswa() async {
    _list.clear();
    setState(() {
      loading = true;
    });
    final response = await http.get(
      BaseURL.getdata_siswa + widget.idsekolah,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + BaseURL.token,
      },
    );
    if (response.contentLength == 2) {
      setState(() {
        loading = false;
      });
    } else {
      final data = jsonDecode(response.body);
      final result = data['data'];
      if (data['status'] == false) {
        _list.clear();
      } else {
        result.forEach(
          (api) {
            final ab = new ModalDataSiswa(
              api['id'],
              api['nisn'],
              api['npsn'],
              api['nik'],
              api['nama'],
              api['alamat'],
              api['sex'],
              api['tgl_lahir'],
              api['email'],
              api['no_hp'],
              api['email_alter'],
              api['nama_orangtua'],
              api['no_orangtua'],
              api['email_orangtua'],
              api['nm_sekolah'],
              api['nama_region'],
              api['xcreate_date'],
            );
            _list.add(ab);
          },
        );
        _search = _list;
      }

      setState(() {
        loading = false;
      });
    }
  }

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  _proseshapus(String nisn) async {
    final response = await http.post(
      BaseURL.siswa_delete_new,
      body: {'nisn': nisn},
      headers: {
        HttpHeaders.authorizationHeader: "Bearer " + BaseURL.token,
      },
    );
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      final result = data['value'];
      final message = data['message'];
      if (result == 1) {
        showToast(message, gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
        _getdata_siswa();
      } else {
        showToast(message, gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      }
    } else {}
  }

  _hapusData(String nisn) {
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          child: ListView(
            padding: EdgeInsets.all(16),
            shrinkWrap: true,
            children: <Widget>[
              Text(
                "Hapus Data Siswa?",
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'monserat',
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Benar Akan Menghapus Data ini?",
                style: TextStyle(
                  fontSize: 16,
                  fontFamily: 'monserat',
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  InkWell(
                      onTap: () {
                        Navigator.pop(context);
                        showDialog(
                          context: context,
                          builder: (context) {
                            Future.delayed(Duration(seconds: 1), () {
                              Navigator.of(context).pop(true);
                              _proseshapus(nisn);
                            });
                            return AlertDialog(
                              title: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  new CircularProgressIndicator(),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  new Text(
                                    "Proses ...",
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontFamily: 'monserat',
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      },
                      child: Text(
                        "Hapus Data",
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'monserat',
                          color: BaseURL.colorGlobal,
                          fontWeight: FontWeight.bold,
                        ),
                      )),
                  SizedBox(
                    width: 16,
                  ),
                  InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Text(
                        "Tidak",
                        style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'monserat',
                        ),
                      )),
                ],
              )
            ],
          ),
        );
      },
    );
  }

  _resetpassword_siswa(String nisn) async {
    final response = await http.post(
      BaseURL.resetpassword_siswa,
      body: {
        'nisn': nisn,
      },
      headers: {
        HttpHeaders.authorizationHeader: "Bearer " + BaseURL.token,
      },
    );
    if (response.statusCode == 200) {
      final data = jsonDecode(response.body);
      final value = data['value'];
      final pesan = data['message'];
      if (value == 1) {
        showToast(pesan, gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      } else {
        showToast(pesan, gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      }
    } else {}
  }

  pesansalah() {
    showToast("Gagal diubah, silahkan coba kembali.",
        gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
  }

  pesanbenar() {
    showToast("Data berhasil diubah.",
        gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
  }

  _setelData(String nisn, String npsn) {
    print(nisn);
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          child: ListView(
            padding: EdgeInsets.all(16),
            shrinkWrap: true,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      // _ubahdataSiswa(nisn);
                      showDialog(
                          context: context,
                          builder: (_) {
                            return DialogUpdateSiswa(
                                nisn, _getdata_siswa, pesanbenar, pesansalah);
                          });
                    },
                    child: Column(
                      children: [
                        Icon(
                          Icons.edit,
                          color: Colors.green,
                        ),
                        Text(
                          "Ubah",
                          style: TextStyle(
                            fontFamily: 'monserat',
                            color: Colors.green,
                          ),
                        ),
                        Text(
                          "Data",
                          style: TextStyle(
                            fontFamily: 'monserat',
                            color: Colors.green,
                          ),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      _hapusData(nisn);
                    },
                    child: Column(
                      children: [
                        Icon(
                          Icons.delete,
                          color: Colors.red,
                        ),
                        Text(
                          "Hapus",
                          style: TextStyle(
                            fontFamily: 'monserat',
                            color: Colors.red,
                          ),
                        ),
                        Text(
                          "Data",
                          style: TextStyle(
                            fontFamily: 'monserat',
                            color: Colors.red,
                          ),
                        ),
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      showDialog(
                        context: context,
                        builder: (context) {
                          Future.delayed(Duration(seconds: 1), () {
                            Navigator.of(context).pop(true);
                            _resetpassword_siswa(nisn);
                          });
                          return AlertDialog(
                            title: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                new CircularProgressIndicator(),
                                SizedBox(
                                  width: 10,
                                ),
                                new Text(
                                  "Proses ...",
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontFamily: 'monserat',
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      );
                    },
                    child: Column(
                      children: [
                        Icon(
                          Icons.lock,
                          color: Colors.orange,
                        ),
                        Text(
                          "Reset",
                          style: TextStyle(
                            fontFamily: 'monserat',
                            color: Colors.orange,
                          ),
                        ),
                        Text(
                          "Password",
                          style: TextStyle(
                            fontFamily: 'monserat',
                            color: Colors.orange,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }

  Future<void> _lihatdata(String id) async {
    final response = await http.get(
      BaseURL.lihatdata_detailsiswa + id,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + BaseURL.token,
      },
    );
    final result = jsonDecode(response.body);
    final data = result['data'];

    idsiswa = TextEditingController(text: data['id']);
    nisn = TextEditingController(text: data['nisn']);
    npsn = TextEditingController(text: data['npsn']);
    nik = TextEditingController(text: data['nik']);
    nama = TextEditingController(text: data['nama']);
    alamat = TextEditingController(text: data['alamat']);
    sex = TextEditingController(text: data['sex']);
    tgl_lahir = TextEditingController(text: data['tgl_lahir']);
    email = TextEditingController(text: data['email']);
    nohp = TextEditingController(text: data['nohp']);
    email_alter = TextEditingController(text: data['email_alter']);
    nama_orangtua = TextEditingController(text: data['nama_orangtua']);
    no_orangtua = TextEditingController(text: data['no_orangtua']);
    email_orangtua = TextEditingController(text: data['email_orangtua']);
    nm_sekolah = TextEditingController(text: data['nm_sekolah']);
    nama_region = TextEditingController(text: data['nama_region']);

    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(32.0))),
          contentPadding: EdgeInsets.only(top: 10.0),
          content: Container(
            width: 400.0,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: 150,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/img/detaildata.png'),
                      fit: BoxFit.fitHeight,
                    ),
                    shape: BoxShape.circle,
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: ListView(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        TextField(
                          controller: idsiswa,
                          decoration: InputDecoration(
                            labelText: "Id",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: nisn,
                          decoration: InputDecoration(
                            labelText: "NISN",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: npsn,
                          decoration: InputDecoration(
                            labelText: "NPSN",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: nik,
                          decoration: InputDecoration(
                            labelText: "NIK",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: nama,
                          decoration: InputDecoration(
                            labelText: "Nama",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: alamat,
                          decoration: InputDecoration(
                            labelText: "Alamat",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: sex,
                          decoration: InputDecoration(
                            labelText: "Jenis Kelamin",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: tgl_lahir,
                          decoration: InputDecoration(
                            labelText: "Tanggal Lahir",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: email,
                          decoration: InputDecoration(
                            labelText: "Email",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: nohp,
                          decoration: InputDecoration(
                            labelText: "No HP",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: email_alter,
                          decoration: InputDecoration(
                            labelText: "Email Alternatif",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: nama_orangtua,
                          decoration: InputDecoration(
                            labelText: "Nama Orang Tua",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: no_orangtua,
                          decoration: InputDecoration(
                            labelText: "No HP Orang Tua",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: email_orangtua,
                          decoration: InputDecoration(
                            labelText: "Email Orang Tua",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: nm_sekolah,
                          decoration: InputDecoration(
                            labelText: "Nama Sekolah",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                        TextField(
                          controller: nama_region,
                          decoration: InputDecoration(
                            labelText: "Regional",
                            labelStyle: TextStyle(
                              fontFamily: 'monserat',
                            ),
                            enabled: false,
                          ),
                          style: TextStyle(
                            fontFamily: 'monserat',
                            fontSize: 15,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  decoration: BoxDecoration(
                    color: BaseURL.colorGlobal,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(32.0),
                      bottomRight: Radius.circular(32.0),
                    ),
                  ),
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      "TUTUP",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'monserat',
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  _listitem(x) {
    return Card(
      color: Colors.transparent,
      elevation: 0,
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(
              color: BaseURL.focusColorTextField,
              width: 5.0,
            ),
          ),
        ),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(0.0),
              bottomLeft: Radius.circular(0.0),
              bottomRight: Radius.circular(5.0),
              topRight: Radius.circular(5.0),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 1,
                offset: Offset(0, 1),
              ),
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        _search[x].nama,
                        maxLines: 1,
                        style: TextStyle(
                          fontFamily: 'monserat',
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        _search[x].nisn + "/" + _search[x].npsn,
                        maxLines: 1,
                        style: TextStyle(
                          fontFamily: 'monserat',
                          fontSize: 12,
                        ),
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.mail,
                            size: 13,
                            color: BaseURL.colorGlobal,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            _search[x].email,
                            style: TextStyle(
                              fontFamily: 'monserat',
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Row(
                  children: [
                    InkWell(
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (context) {
                            Future.delayed(Duration(seconds: 1), () {
                              Navigator.of(context).pop(true);
                              _lihatdata(_search[x].id);
                            });
                            return AlertDialog(
                              title: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  new CircularProgressIndicator(),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  new Text(
                                    "Mengambil data ...",
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontFamily: 'monserat',
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      },
                      child: Column(
                        children: [
                          Icon(
                            Icons.remove_red_eye,
                            size: 15,
                            color: BaseURL.colorGlobal,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "Lihat",
                            maxLines: 1,
                            style: TextStyle(
                              fontFamily: 'monserat',
                              fontSize: 10,
                            ),
                          ),
                          Text(
                            "Data",
                            maxLines: 1,
                            style: TextStyle(
                              fontFamily: 'monserat',
                              fontSize: 10,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    InkWell(
                      onTap: () {
                        _setelData(_search[x].nisn, _search[x].npsn);
                      },
                      child: Column(
                        children: [
                          Icon(
                            Icons.edit,
                            size: 15,
                            color: BaseURL.colorGlobal,
                          ),
                          SizedBox(
                            height: 5,
                          ),
                          Text(
                            "Setel",
                            maxLines: 1,
                            style: TextStyle(
                              fontFamily: 'monserat',
                              fontSize: 10,
                            ),
                          ),
                          Text(
                            "Data",
                            maxLines: 1,
                            style: TextStyle(
                              fontFamily: 'monserat',
                              fontSize: 10,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _hasilinfo() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(32.0))),
          contentPadding: EdgeInsets.only(top: 10.0),
          content: Container(
            width: 400.0,
            height: 220,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: 100,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/img/confirm_check.png'),
                      fit: BoxFit.fitHeight,
                    ),
                    shape: BoxShape.circle,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: ListView(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Center(
                        child: Column(
                          children: [
                            Text(
                              "Password berhasil direset.",
                              style: TextStyle(
                                fontFamily: 'monserat',
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                  decoration: BoxDecoration(
                    color: BaseURL.colorGlobal,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(32.0),
                      bottomRight: Radius.circular(32.0),
                    ),
                  ),
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      "TUTUP",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'monserat',
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _getdata_siswa();
  }

  _searchBar() {
    return TextField(
      // controller: controller,
      // onChanged: onSearch,
      decoration: new InputDecoration(
        hintText: "Cari ...",
        hintStyle: TextStyle(
          fontFamily: 'monserat',
          fontSize: 15,
        ),
        fillColor: Colors.white,
        border: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(50.0),
          borderSide: new BorderSide(),
        ),
        suffixIcon: IconButton(
          icon: Icon(
            Icons.search,
          ),
          onPressed: () {
            // _mencaridata(ktp.text);
          },
        ),
      ),
      keyboardType: TextInputType.text,
      style: new TextStyle(
        fontFamily: 'monserat',
        fontSize: 15,
      ),
      onChanged: (text) {
        text = text.toLowerCase();
        setState(() {
          _search = _list.where((note) {
            var noteTitle = note.nama.toLowerCase();
            return noteTitle.contains(text);
          }).toList();
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: Colors.black,
      ));
      statusbar = "0";
    } else if (Platform.isIOS) {
      // iOS-specific code
      statusbar = "1";
    }
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: BaseURL.colorGlobal,
      ),
      home: Scaffold(
        appBar: AppBar(
          brightness: statusbar == "1" ? Brightness.light : Brightness.dark,
          backgroundColor: Colors.white,
          toolbarHeight: 60,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      height: 25,
                      width: 25,
                      decoration: BoxDecoration(
                        color: BaseURL.colorGlobal,
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.black54,
                              blurRadius: 3.0,
                              offset: Offset(0.5, 1))
                        ],
                        borderRadius: BorderRadius.circular(50.0),
                      ),
                      child: Center(
                        child: Icon(
                          Icons.arrow_back,
                          size: 18,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "Data Siswa",
                            style: TextStyle(
                              fontSize: 18.0,
                              fontFamily: 'monserat',
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        body: RefreshIndicator(
          onRefresh: _getdata_siswa,
          key: _refresh,
          child: loading || _list.length == 0
              ? ListView.builder(
                  itemCount: 1,
                  itemBuilder: (ctx, i) {
                    return Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 100),
                          child: Center(
                            child: new Container(
                                width: 120.00,
                                height: 100.00,
                                decoration: new BoxDecoration(
                                  image: new DecorationImage(
                                    image: ExactAssetImage(
                                        'assets/img/nodata.png'),
                                    fit: BoxFit.fitHeight,
                                  ),
                                )),
                          ),
                        ),
                        Text(
                          "Data Kosong",
                          style: TextStyle(
                            fontFamily: 'monserat',
                          ),
                        ),
                      ],
                    );
                  })
              : Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            return index == 0
                                ? _searchBar()
                                : _listitem(index - 1);
                          },
                          itemCount: _search.length + 1,
                        ),
                      )
                    ],
                  ),
                ),
        ),
      ),
    );
  }
}
