import 'dart:convert';
import 'dart:io';

import 'package:apps_zimbraadmin/modal/api.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:toast/toast.dart';

class DialogUpdateSiswa extends StatefulWidget {
  final String nisn;
  final VoidCallback reload;
  final VoidCallback pesanbenar;
  final VoidCallback pesansalah;
  DialogUpdateSiswa(this.nisn, this.reload, this.pesanbenar, this.pesansalah);
  @override
  _DialogUpdateSiswaState createState() => _DialogUpdateSiswaState();
}

class _DialogUpdateSiswaState extends State<DialogUpdateSiswa> {
  TextEditingController datanisn = new TextEditingController();
  TextEditingController datanpsn = new TextEditingController();
  TextEditingController asalsekolah = new TextEditingController();
  TextEditingController kelas = new TextEditingController();
  TextEditingController namasiswa = new TextEditingController();
  var loading = false;

  void showToast(String msg, {int duration, int gravity}) {
    Toast.show(msg, context, duration: duration, gravity: gravity);
  }

  _ubahdataSiswa(String nisn) async {
    setState(() {
      loading = true;
    });
    try {
      final response = await http.get(
        BaseURL.show_siswa + nisn,
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + BaseURL.token,
        },
      );
      if (response.statusCode == 200) {
        final data = jsonDecode(response.body);
        final result = data['data'];
        datanisn = TextEditingController(text: result['nisn']);
        datanpsn = TextEditingController(text: result['npsn']);
        asalsekolah = TextEditingController(text: result['nm_sekolah']);
        kelas = TextEditingController(text: result['kelas']);
        namasiswa = TextEditingController(text: result['nama']);
      } else {
        showToast("Maaf, ada kesalahan di server.",
            gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
      }
    } catch (e) {
      showToast("Maaf, ada kesalahan di server.",
          gravity: Toast.CENTER, duration: Toast.LENGTH_LONG);
    }
    setState(() {
      loading = false;
    });
  }

  _updatedata(String nisn, String kelas, String nama) async {
    final response = await http.post(
      BaseURL.siswa_update_new,
      body: {
        'nisn': nisn,
        'kelas': kelas,
        'nama': nama,
      },
      headers: {
        HttpHeaders.authorizationHeader: "Bearer " + BaseURL.token,
      },
    );
    final data = jsonDecode(response.body);
    final value = data['value'];
    if (value == 1) {
      widget.reload();
      widget.pesanbenar();
    } else {
      widget.pesansalah();
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _ubahdataSiswa(widget.nisn);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: [
          Column(
            children: [
              AlertDialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0))),
                title: Text(
                  'Ubah Data Siswa',
                  style: TextStyle(
                    fontFamily: 'Quicksand',
                  ),
                ),
                content: Column(children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: new BorderRadius.circular(15.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(1),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 3, bottom: 3, left: 10, right: 10),
                          child: TextFormField(
                            readOnly: true,
                            controller: datanisn,
                            style: new TextStyle(
                              fontFamily: 'monserat',
                            ),
                            keyboardType: TextInputType.text,
                            autofocus: false,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "NISN",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: new BorderRadius.circular(15.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(1),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 3, bottom: 3, left: 10, right: 10),
                          child: TextFormField(
                            readOnly: true,
                            controller: datanpsn,
                            style: new TextStyle(
                              fontFamily: 'monserat',
                            ),
                            keyboardType: TextInputType.text,
                            autofocus: false,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "NPSN",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: new BorderRadius.circular(15.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(1),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 3, bottom: 3, left: 10, right: 10),
                          child: TextFormField(
                            readOnly: true,
                            controller: asalsekolah,
                            style: new TextStyle(
                              fontFamily: 'monserat',
                            ),
                            keyboardType: TextInputType.text,
                            autofocus: false,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Asal Sekolah",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.circular(15.0),
                        border: Border.all(
                          color: Colors.black,
                          width: 1,
                        ),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(1),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 3, bottom: 3, left: 10, right: 10),
                          child: TextFormField(
                            controller: kelas,
                            style: new TextStyle(
                              fontFamily: 'monserat',
                            ),
                            keyboardType: TextInputType.text,
                            autofocus: false,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Kelas",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: new BorderRadius.circular(15.0),
                        border: Border.all(
                          color: Colors.black,
                          width: 1,
                        ),
                      ),
                      child: Padding(
                        padding: EdgeInsets.all(1),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              top: 3, bottom: 3, left: 10, right: 10),
                          child: TextFormField(
                            controller: namasiswa,
                            style: new TextStyle(
                              fontFamily: 'monserat',
                            ),
                            keyboardType: TextInputType.text,
                            autofocus: false,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "Nama",
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Batal",
                            style: TextStyle(
                              fontSize: 16,
                              fontFamily: 'monserat',
                            ),
                          )),
                      SizedBox(
                        width: 16,
                      ),
                      InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            showDialog(
                              context: context,
                              builder: (context) {
                                Future.delayed(Duration(seconds: 1), () {
                                  Navigator.of(context).pop(true);
                                  _updatedata(datanisn.text, kelas.text,
                                      namasiswa.text);
                                });
                                return AlertDialog(
                                  title: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      new CircularProgressIndicator(),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      new Text(
                                        "Proses ...",
                                        style: TextStyle(
                                          fontSize: 15,
                                          fontFamily: 'monserat',
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            );
                          },
                          child: Text(
                            "Perbarui",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              fontFamily: 'monserat',
                              color: BaseURL.colorGlobal,
                            ),
                          )),
                    ],
                  )
                ]),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
